<?php
	include 'includes/session.php';

	$id = $_POST['id'];

	$conn = $pdo->open();

	$output = array('list'=>'');

	$stmt = $conn->prepare("SELECT * FROM details LEFT JOIN products ON products.id=details.product_id LEFT JOIN orders ON orders.orders_id=details.sales_id  WHERE details.sales_id=:id");
	$stmt->execute(['id'=>$id]);
	$total = 0;
	foreach($stmt as $row){
		$stmt2 = $conn->prepare("SELECT * FROM users WHERE id=:id");
		$stmt2->execute(['id'=>$row['user_id']]);
		foreach($stmt2 as $row2){
			$fullname = $row2['firstname'] . ' ' . $row2['lastname'];
		}
		$output['orders_id'] = $row['orders_id'];
		$output['user_email'] = $row2['email'];
		$output['id'] = $row['user_id'];
		$output['name'] = $fullname;
		$output['order_type'] = $row['order_type'];
		$output['address'] = $row['address'];
		$output['company'] = $row['company'];
		$output['transaction'] = $row['pay_id'];
		$output['confirmed_by'] = $row['confirmed_by'];
		$output['date'] = date('M d, Y', strtotime($row['order_date']));
		$subtotal = $row['price']*$row['quantity'];
		$total += $subtotal;
		$output['list'] .= "
			<tr class='prepend_items'>
				<td>".$row['name']."</td>
				<td>&#8369; ".number_format($row['price'], 2)."</td>
				<td>".$row['quantity']."</td>
				<td>".$row['order_type']."</td>
				<td>&#8369; ".number_format($subtotal, 2)."</td>
			</tr>
		";
	}
	
	$output['total'] = '<b>&#8369; '.number_format($total, 2).'<b>';
	$pdo->close();
	echo json_encode($output);

?>