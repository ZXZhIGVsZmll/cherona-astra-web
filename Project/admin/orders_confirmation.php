<?php
	// PHPMailer
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	require '../vendor/autoload.php';
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->SMTPAuth = true;
	$mail->Username = 'cheronaastra@gmail.com';
	$mail->Password = 'The67012yis@llYOurS';
	
	include 'includes/session.php';

	$adminId = $admin['lastname'];
	$order_type = $_POST['order_type'];
	$user_id = $_POST['user_id'];
	$email = $_POST['email'];
	$fullname = $_POST['name'];
	$id = $_POST['orders_id'];
	$transId = $_POST['trans_id'];
	$date = date('Y-m-d');
	
	// Email content
	$mail->setFrom('cheronaastra@gmail.com', 'Cherona Astra');
	$mail->addReplyTo('cheronaastra@gmail.com', 'Cherona Astra');
	$mail->addAddress($email, $fullname);
	$mail->Subject = 'Order Approved';
	$mail->msgHTML(file_get_contents('../vendor/phpmailer/mail-content/order-approved.html'), __DIR__);
	
	if($order_type=='pick-up'){
		//PAID (PICK-UP)
		if(isset($_POST['paid'])){
			$conn = $pdo->open();
			try{
				$stmt = $conn->prepare("UPDATE orders SET order_status=:order_status, confirmed_by=:confirmed_by WHERE orders_id=:id");
				$stmt->execute(['order_status'=>"paid_pick-up", 'confirmed_by'=>$adminId, 'id'=>$id]);

				$stmt2 = $conn->prepare("INSERT INTO sales (orders_id, user_id, pay_id, sales_date) VALUES (:orders_id, :user_id, :pay_id, :sales_date)");
				$stmt2->execute(['orders_id'=>$id, 'user_id'=>$user_id, 'pay_id'=>$transId, 'sales_date'=>$date]);
				$salesid = $conn->lastInsertId();

				$_SESSION['paid_pick-up'] = 'Order ID: '.$id.' <br> Transaction #: '.$transId.'';
				$mail->send(); // Send email
			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}
			$pdo->close();
		}
		//REJECT (PICK-UP)
		if(isset($_POST['reject'])){
			$conn = $pdo->open();
			try{
				$stmt = $conn->prepare("UPDATE orders SET order_status=:order_status, confirmed_by=:confirmed_by WHERE orders_id=:id");
				$stmt->execute(['order_status'=>"reject", 'confirmed_by'=>$adminId, 'id'=>$id]);
				$_SESSION['rejected'] = 'Order ID: '.$id.' <br> Transaction #: '.$transId.'';
			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}
			$pdo->close();
		}
	}else{
		//PAID (SHIP)
		if(isset($_POST['paid'])){
			$conn = $pdo->open();
			try{
				$stmt = $conn->prepare("UPDATE orders SET order_status=:order_status, confirmed_by=:confirmed_by WHERE orders_id=:id");
				$stmt->execute(['order_status'=>"paid", 'confirmed_by'=>$adminId, 'id'=>$id]);
				$_SESSION['paid'] = 'Order ID: '.$id.' <br> Transaction #: '.$transId.'';

				$stmt2 = $conn->prepare("INSERT INTO sales (orders_id, user_id, pay_id, sales_date) VALUES (:orders_id, :user_id, :pay_id, :sales_date)");
				$stmt2->execute(['orders_id'=>$id, 'user_id'=>$user_id, 'pay_id'=>$transId, 'sales_date'=>$date]);
				$salesid = $conn->lastInsertId();
				$mail->send(); // Send email
			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}
			$pdo->close();
		}
		//REJECT (SHIP)
		if(isset($_POST['reject'])){
			$conn = $pdo->open();
			try{
				$stmt = $conn->prepare("UPDATE orders SET order_status=:order_status, confirmed_by=:confirmed_by WHERE orders_id=:id");
				$stmt->execute(['order_status'=>"reject", 'confirmed_by'=>$adminId, 'id'=>$id]);
				$_SESSION['rejected'] = 'Order ID: '.$id.' <br> Transaction #: '.$transId.'';
			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}
			$pdo->close();
		}
		//DELIVERED (SHIP)
		if(isset($_POST['delivered'])){
			$conn = $pdo->open();
			try{
				$stmt = $conn->prepare("UPDATE orders SET order_status=:order_status, confirmed_by=:confirmed_by WHERE orders_id=:id");
				$stmt->execute(['order_status'=>"delivered", 'confirmed_by'=>$adminId, 'id'=>$id]);
				
				$_SESSION['delivered'] = 'Order ID: '.$id.' <br> Transaction #: '.$transId.' ';
			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}
			$pdo->close();
		}
	}
	header('location: orders.php');
?>