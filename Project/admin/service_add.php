<?php
	include 'includes/session.php';
	include 'includes/slugify.php';

	if(isset($_POST['add'])){
		$name = $_POST['name'];
		$slug = slugify($name);
		$category = $_POST['category'];
		$service_admin = $_POST['service_admin'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$filename = $_FILES['photo']['name'];

		$conn = $pdo->open();

		$stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM services WHERE slug=:slug");
		$stmt->execute(['slug'=>$slug]);
		$row = $stmt->fetch();

		if($row['numrows'] > 0){
			$_SESSION['error'] = 'Service already exist';
		}
		else{
			if(!empty($filename)){
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$new_filename = $slug.'.'.$ext;
				move_uploaded_file($_FILES['photo']['tmp_name'], '../images/'.$new_filename);	
			}
			else{
				$new_filename = '';
			}

			$stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM services WHERE service_admin_id=:service_admin_id");
			$stmt->execute(['service_admin_id'=>$service_admin]);
			$row = $stmt->fetch();

			if($row['numrows'] > 0){
				$_SESSION['error'] = 'Admin account already selected. Create new Service Admin <a href="serv_users.php">here</a>';
			}else{
				try{
					$stmt = $conn->prepare("INSERT INTO services (category_id, service_admin_id, name, description, slug, price, photo) VALUES (:category, :service_admin_id, :name, :description, :slug, :price, :photo)");
					$stmt->execute(['category'=>$category, 'service_admin_id'=>$service_admin, 'name'=>$name, 'description'=>$description, 'slug'=>$slug, 'price'=>$price, 'photo'=>$new_filename]);
					$_SESSION['success'] = 'User added successfully';
	
				}
				catch(PDOException $e){
					$_SESSION['error'] = $e->getMessage();
				}
			}
		}

		$pdo->close();
	}
	else{
		$_SESSION['error'] = 'Fill up services form first';
	}

	header('location: service.php');

?>