<?php
	include 'includes/session.php';
	include 'includes/slugify.php';

	if(isset($_POST['update_admin'])){
        $id = $_POST['id'];
		$service_admin = $_POST['service_admin'];
        $service_name = $_POST['service_name'];

        $conn = $pdo->open();

        $stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM services WHERE service_admin_id=:service_admin_id");
		$stmt->execute(['service_admin_id'=>$service_admin]);
		$row = $stmt->fetch();

		if($row['numrows'] > 0){
			$_SESSION['error'] = 'Admin account already selected create new user <a href="serv_users.php">here</a>';
		}else{
			try{
				$stmt = $conn->prepare("UPDATE services SET service_admin_id=:service_admin_id WHERE id=:id");
				$stmt->execute(['service_admin_id'=>$service_admin, 'id'=>$id]);
				$_SESSION['success'] = "$service_name's Service Admin Updated Successfully";
			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}
		}
		$pdo->close();
	}
	else{
		$_SESSION['error'] = 'Error! Cannot update the Admin';
	}
	header('location: service.php');
?>