<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Booking Sales History
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Booking Sales</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered" data-order='[[0,"desc"]]'>
                <thead>
                  <th>Sales ID</th>
                  <th>Sales Date</th>
                  <th>Booking Transaction #</th>
                  <th>Service Name</th>
                  <th>Customer Name</th>
                  <th>Full Details</th>

                </thead>
                <tbody>
                <?php
                    $conn = $pdo->open();

                    try{
                      $stmt = $conn->prepare("SELECT *, booking_sales.id AS booksales_id FROM booking_sales LEFT JOIN bookings ON bookings.id=booking_sales.booking_id LEFT JOIN services ON bookings.services_id=services.id ORDER BY booking_sales.id DESC");
                      $stmt->execute();
                      foreach($stmt as $row){
                        $stmt = $conn->prepare("SELECT * FROM users WHERE id=:customer_id");
                        $stmt->execute(['customer_id'=>$row['customer_id']]);
                        foreach($stmt as $cu_details){
                          $fullname = $cu_details['firstname'].' '.$cu_details['lastname'];
                        }
                        echo "
                          <tr>
                            <td>".$row['booksales_id']."</td>
                            <td>".$row['sales_date']."</td>
                            <td>".$row['bookingtrans']."</td>
                            <td>".$row['name']."</td>
                            <td>".$fullname."</td>
                            <td><button type='button' class='btn btn-info btn-sm btn-flat booktransact' data-id='".$row['booksales_id']."'><i class='fa fa-search'></i> View</button></td>
                          </tr>
                        ";
                      }
                    }
                    catch(PDOException $e){
                      echo $e->getMessage();
                    }

                    $pdo->close();
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     
  </div>
  	<?php include 'includes/footer.php'; ?>
    <?php include 'includes/bookings_modal.php'; ?>

</div>
<!-- ./wrapper -->

<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
  $(document).on('click', '.booktransact', function(e){
    e.preventDefault();
    $('#service_details').modal('show');
    var id = $(this).data('id');
    $.ajax({
      type: 'POST',
      url: 'booking_sales_details.php',
      data: {id:id},
      dataType: 'json',
      success:function(response){
        $('#address').html(response.address);
        $('#customer_name').html(response.fullname);
        $('#sales_date').html(response.sales_date);
        $('#booktrans_id').html(response.booktrans_id);
        $('#customer_contact').html(response.contact);
        $('#cherona_admin').html(response.confirmed_by);
        $('#total').html(response.total);
        $('#detail').prepend(response.list);
      }
    });
  });

  $("#service_details").on("hidden.bs.modal", function () {
      $('.prepend_items').remove();
  });
});
</script>
</body>
</html>
