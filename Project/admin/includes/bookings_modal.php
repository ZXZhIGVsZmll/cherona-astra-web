<!-- Paid -->
<div class="modal fade" id="paid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-paid">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Booking Service has been Paid?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="bookings_confirmation.php">
              <p>
                Booked ID: <span id="bookid_show"></span>
                <span class="pull-right">Booking Date: <span id="booking_date"></span></span>
              </p>
              <p>
                Service Name: <span id="service_name"></span>
                <span class="pull-right">Amount: <span id="price"></span></span>
              </p>
                <input type="hidden" id="services_id" name="services_id">
                <input type="hidden" id="bookid" name="bookid">
                <input type="hidden" id="booktransaction" name="booktransaction">
                <input type="hidden" id="fullname" name="fullname">
                <div class="text-center">
                    <p> </p>
                    <h2 class="bold fullname"></h2>
                    <h2 class="lead booktransaction"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-paid-book btn-flat" name="paid"><i class="fa fa-check"></i> Paid</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Transaction History -->
<div class="modal fade" id="service_details">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Booked Service Full Details</b></h4>
            </div>
            <div class="modal-body">
              <p>
                Sales Date: <span id="sales_date"></span>
                <span class="pull-right">Transaction #: <span id="booktrans_id"></span></span> 
              </p>
              <p>
                Customer Name: <span id="customer_name"></span>
                <span class="pull-right">Customer Contact: <span id="customer_contact"></span></span> 
              </p>
              <p>
                Address: <span id="address"></span>
              </p>
              <table class="table table-bordered">
                  <thead>
                    <th>Booking Date</th>
                    <th>Service Name</th>
                    <th>Amount</th>
                  </thead>
                  <tbody id="detail">
                    <tr>
                      <td colspan="2" align="right"><b>Total Amount</b></td>
                      <td><span id="total"></span></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <p>
                  <span class="pull-right">Payment Confirmed: <span id="cherona_admin"></span></span>
                </p>
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>