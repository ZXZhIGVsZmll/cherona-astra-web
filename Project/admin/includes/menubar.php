<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo (!empty($admin['photo'])) ? '../images/'.$admin['photo'] : '../images/profile.jpg'; ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $admin['firstname'].' '.$admin['lastname']; ?></p>
        <a><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">REPORTS</li>
      <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li><a href="sales.php"><i class="fa fa-money"></i> <span>Sales</span></a></li>
      <li><a href="booking_sales.php"><i class="fa fa-money"></i> <span>Services Sales</span></a></li>
      <li class="header">MANAGE</li>
      <li><a href="users.php"><i class="fa fa-users"></i> <span>Users</span>
        <small class="badge pull-right bg-red" data-toggle="tooltip" data-placement="top" title="Not Verified">
          <?php
            $stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM users WHERE status='0'");
            $stmt->execute();
            $urow =  $stmt->fetch();

            echo "".$urow['numrows']."";
          ?>
        </small>
      </a>
      </li>
	  <li><a href="serv_users.php"><i class="fa fa-users"></i> <span>Service Users</span></a></li>
    <li>
      <a href="orders.php">
        <i class="fa fa-shopping-cart"></i> <span>Orders</span>
        <small class="badge pull-right bg-orange" data-toggle="tooltip" data-placement="top" title="Pending Orders">
          <?php
            $stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM orders WHERE order_status='pending'");
            $stmt->execute();
            $urow =  $stmt->fetch();

            echo "".$urow['numrows']."";
          ?>
        </small>
        <small class="badge pull-right bg-dark-deliver" data-toggle="tooltip" data-placement="top" title="Not yet Delivered Orders">
          <?php
            $stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM orders WHERE order_status='paid'");
            $stmt->execute();
            $urow =  $stmt->fetch();

            echo "".$urow['numrows']."";
          ?>
        </small>
      </a>
    </li>
    <li>
      <a href="bookings.php"><i class="fa fa-calendar"></i> <span>Bookings</span>
        <small class="badge pull-right bg-orange" data-toggle="tooltip" data-placement="top" title="Pending Bookings">
          <?php
            $stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM bookings WHERE booking_status='pending'");
            $stmt->execute();
            $urow =  $stmt->fetch();

            echo "".$urow['numrows']."";
          ?>
        </small>
        <small class="badge pull-right bg-booked-book" data-toggle="tooltip" data-placement="top" title="Booked Services">
          <?php
            $stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM bookings WHERE booking_status='booked'");
            $stmt->execute();
            $urow =  $stmt->fetch();

            echo "".$urow['numrows']."";
          ?>
        </small>
      </a>
    </li>
	  <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Service</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="service.php"><i class="fa fa-circle-o"></i> Service List</a></li>
          <li><a href="serv_category.php"><i class="fa fa-circle-o"></i> Category</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-barcode"></i>
          <span>Products</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="products.php"><i class="fa fa-circle-o"></i> Product List</a></li>
          <li><a href="category.php"><i class="fa fa-circle-o"></i> Category</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>