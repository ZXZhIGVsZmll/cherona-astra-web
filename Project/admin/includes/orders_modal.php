<!-- Order Details -->
<div class="modal fade" id="transaction">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Order Full Details</b></h4>
            </div>
            <div class="modal-body">
              <p>
                Name: <span id="name"></span>
                <span class="pull-right">Customer ID: <span id="id"></span></span>
              </p>
              <p>
                Order Type: <span id="order_type"></span>
              </p>
              <p>
                Address: <span id="address"></span>
                <span class="pull-right">Company: <span id="company"></span></span>
              </p>
              <p>
                Date: <span id="date"></span>
                <span class="pull-right">Transaction#: <span id="transid"></span></span>
              </p>
              <table class="table table-bordered">
                <thead>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Order Type</th>
                  <th>Subtotal</th>
                </thead>
                <tbody id="detail">
                  <tr>
                    <td colspan="4" align="right"><b>Total</b></td>
                    <td><span id="total"></span></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <p>Confirmed by: <span id="confirmed_by"></span>
              </p>
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Reject -->
<div class="modal fade" id="reject">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Reject this Order?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="orders_confirmation.php">
                <input type="hidden" id="orders_id" name="orders_id">
                <input type="hidden" id="trans_id" name="trans_id">
                <div class="text-center">
                    <p>REJECT ORDER?</p>
                    <h2 class="bold orders_id"></h2>
                    <h2 class="lead trans_id"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-danger btn-flat" name="reject"><i class="fa fa-close"></i> Reject</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Paid -->
<div class="modal fade" id="paid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Order has been Paid?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="orders_confirmation.php">
                <input type="hidden" id="paid_user_id" name="user_id">
                <input type="hidden" id="paid_orders_id" name="orders_id">
                <input type="hidden" id="paid_trans_id" name="trans_id">
                <input type="hidden" id="paid_order_type" name="order_type">
                <input type="hidden" id="paid_email" name="email">
				<input type="hidden" id="fullname" name="name">
                <div class="text-center">
                    <p>ORDER PAID?</p>
                    <h2 class="bold orders_id"></h2>
                    <h2 class="lead trans_id"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="paid"><i class="fa fa-check"></i> Paid</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Delivered -->
<div class="modal fade" id="delivered">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Order has been Delivered?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="orders_confirmation.php">
                <input type="hidden" id="delivered_user_id" name="user_id">
                <input type="hidden" id="delivered_orders_id" name="orders_id">
                <input type="hidden" id="delivered_trans_id" name="trans_id">
                <div class="text-center">
                    <p>ORDER ALREADY DELIVERED?</p>
                    <h2 class="bold orders_id"></h2>
                    <h2 class="lead trans_id"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success-deliver btn-flat" name="delivered"><i class="fa fa-truck"></i> Delivered</button>
              </form>
            </div>
        </div>
    </div>
</div>