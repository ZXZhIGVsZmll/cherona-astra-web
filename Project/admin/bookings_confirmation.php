<?php
	include 'includes/session.php';

	$adminId = $admin['lastname'];
	$bookid = $_POST['bookid'];
	$service_id = $_POST['services_id'];
    $fullname = $_POST['fullname'];
	$booktransaction = $_POST['booktransaction'];
	$date = date('Y-m-d');

    if(isset($_POST['paid'])){
        $conn = $pdo->open();
        try{
            $stmt = $conn->prepare("UPDATE bookings SET booking_status=:booking_status, confirmed_by=:confirmed_by WHERE id=:id");
            $stmt->execute(['booking_status'=>"paid", 'confirmed_by'=>$adminId, 'id'=>$bookid]);

            $stmt2 = $conn->prepare("INSERT INTO booking_sales (booking_id, sales_service_id, sales_date) VALUES (:booking_id, :sales_id, :sales_date)");
            $stmt2->execute(['booking_id'=>$bookid, 'sales_id'=>$service_id, 'sales_date'=>$date]);
            $salesid = $conn->lastInsertId();

            $_SESSION['paid'] = 'Booked ID: '.$bookid.' <br> Booking Transaction #: '.$booktransaction.' <br> Customer Name: '.$fullname.' ';
        }
        catch(PDOException $e){
            $_SESSION['error'] = $e->getMessage();
        }
        $pdo->close();
    }

	header('location: bookings.php');
?>