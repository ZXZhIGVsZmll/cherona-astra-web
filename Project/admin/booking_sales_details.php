<?php
	include 'includes/session.php';

	$id = $_POST['id'];

	$conn = $pdo->open();

	$output = array('list'=>'');

    $stmt = $conn->prepare("SELECT *, booking_sales.id AS booksales_id FROM booking_sales LEFT JOIN bookings ON bookings.id=booking_sales.booking_id LEFT JOIN services ON bookings.services_id=services.id WHERE booking_sales.id=:id");
    $stmt->execute(['id'=>$id]);
    foreach($stmt as $row){
      $stmt = $conn->prepare("SELECT * FROM users WHERE id=:customer_id");
      $stmt->execute(['customer_id'=>$row['customer_id']]);
      foreach($stmt as $cu_details){
        $fullname = $cu_details['firstname'].' '.$cu_details['lastname'];
      }
      $output['sales_date'] = date('M d, Y', strtotime($row['sales_date']));
      $output['address'] = $row['address'];
      $output['booktrans_id'] = $row['bookingtrans'];
      $output['fullname'] = $fullname;
      $output['contact'] = $row['phone'];
      $output['confirmed_by'] = $row['confirmed_by'];
      $total = $row['price'];
      $output['list'] .= "
          <tr class='prepend_items'>
              <td>".date('M d, Y', strtotime($row['date']))."</td>
              <td>".$row['name']."</td>
              <td>&#8369;".number_format($total, 2)."</td>
          </tr>
      ";
    }
	$output['total'] = '<b>&#8369;'.number_format($total, 2).'<b>';
	$pdo->close();
	echo json_encode($output);

?>