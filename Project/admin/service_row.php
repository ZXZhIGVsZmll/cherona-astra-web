<?php 
	include 'includes/session.php';

	if(isset($_POST['id'])){
		$id = $_POST['id'];
		
		$conn = $pdo->open();

		$stmt = $conn->prepare("SELECT *, services.id AS prodid, services.name AS prodname, service_category.name AS catname, users.id AS serviceadmin_id, CONCAT(users.firstname, ' ', users.lastname) AS fullname FROM services LEFT JOIN service_category ON service_category.id=services.category_id LEFT JOIN users ON users.id=services.service_admin_id WHERE services.id=:id");
		$stmt->execute(['id'=>$id]);
		$row = $stmt->fetch();
		
		$pdo->close();

		echo json_encode($row);
	}
?>