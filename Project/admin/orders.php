<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Orders
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php
        //PAID
        if(isset($_SESSION['paid'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success! Order Paid.</h4>
              ".$_SESSION['paid']."
            </div>
          ";
          include "includes/order_paid.php";
          unset($_SESSION['paid']);
        }
        //PAID (PICK-UP)
        if(isset($_SESSION['paid_pick-up'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success! Order Paid.</h4>
              ".$_SESSION['paid_pick-up']."
            </div>
          ";
          include "includes/order_paid_pick-up.php";
          unset($_SESSION['paid_pick-up']);
        }
        //DELIVERED
        if(isset($_SESSION['delivered'])){
          echo "
            <div class='alert alert-success-deliver alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success! Order Delivered.</h4>
              ".$_SESSION['delivered']."
            </div>
          ";
          include "includes/order_delivered.php";
          unset($_SESSION['delivered']);
        }
        //REJECTED
        if(isset($_SESSION['rejected'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['rejected']."
            </div>
          ";
          include "includes/order_rejected.php";
          unset($_SESSION['rejected']);
        }
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered" data-order='[[ 0, "desc"]]'>
                <thead>
                  <th>ID</th>
                  <th>Transaction #</th>
                  <th>Name</th>
                  <th>Contact #</th>
                  <th>Order Type</th>
                  <th>Status</th>
                  <th>Order Date</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $conn = $pdo->open();

                    try{
                      $stmt = $conn->prepare("SELECT * FROM orders LEFT JOIN users ON users.id=orders.user_id ORDER BY orders.orders_id DESC");
                      $stmt->execute();
                      foreach($stmt as $row){
                        echo "
                          <tr>
                            <td>".$row['orders_id']."</td>
                            <td>".$row['pay_id']."</td>
                            <td>
                              ".$row['firstname'].' '.$row['lastname']."
                            </td>
                            <td>".$row['contacts']."</td>
                            <td>";
                            if($row['order_type']=='pick-up'){
                              echo '<span class="label label-default">pick-up</span>';
                            }else{
                              echo '<span class="label label-primary">ship</span>';
                            }
                            
                            echo"</td>
                            <td>
                              ";
                              if($row['order_status']=='pending'){
                                echo '<span class="label label-warning">pending</span>  
                                ';
                              }elseif ($row['order_status']=='paid') {
                                echo '<span class="label label-success">paid</span> <span class="label label-danger-deliver">not yet delivered</span>';
                              }elseif ($row['order_status']=='paid_pick-up') {
                                echo '<span class="label label-success">paid</span>';
                              }elseif ($row['order_status']=='delivered') {
                                echo '<span class="label label-success">paid</span> <span class="label label-success-deliver">delivered</span>';
                              }
                              else{
                                echo '<span class="label label-danger">rejected</span>';
                              }
                               
                              echo "
                            </td>
                            <td>".date('M d, Y', strtotime($row['order_date']))."</td>
                            <td><button class='btn btn-sm btn-flat btn-info order_details' data-id='".$row['orders_id']."'><i class='fa fa-search'></i> Details</button>";
                            echo '
                                  
                                  ';
                                  //Button for Reject
                                  if ($row['order_status']=='pending'){
                                    echo '<button class="btn btn-sm btn-danger btn-flat reject" data-id="'.$row['orders_id'].'" name="reject"><i class="fa fa-close"></i> Reject</button>';
                                  }else{
                                    echo '<button disabled class="btn btn-sm btn-disabled btn-flat"><i class="fa fa-close"></i> Reject</button>';
                                  }

                                  //SHIP OR STORE PICK-UP
                                  if($row['order_type']=='ship'){
                                      //Button for Paid and delivered (ship)
                                    if ($row['order_status']=='paid'){
                                      echo '<button class="btn btn-sm btn-success-deliver btn-flat delivered" data-id="'.$row['orders_id'].'" name="delivered"><i class="fa fa-truck"></i> Delivered</button>';
                                    }elseif($row['order_status']=='reject'){
                                      echo '<button disabled class="btn btn-sm btn-disabled btn-flat"><i class="fa fa-truck"></i> Delivered</button>
                                      ';
                                    }elseif($row['order_status']=='delivered'){
                                      echo '<button disabled class="btn btn-sm btn-disabled btn-flat" ><i class="fa fa-truck"></i> Delivered</button>
                                      ';
                                    }else{
                                      echo '<button class="btn btn-sm btn-success btn-flat paid" data-id="'.$row['orders_id'].'" name="paid"><i class="fa fa-check"></i> Paid</button>';
                                    }
                                  }else{
                                      //Button for Paid (pick-up)
                                    if ($row['order_status']=='pending'){
                                      echo '<button class="btn btn-sm btn-success btn-flat paid" data-id="'.$row['orders_id'].'" name="paid"><i class="fa fa-check"></i> Paid</button>';
                                    }else{
                                      echo '<button disabled class="btn btn-sm btn-disabled btn-flat"><i class="fa fa-check"></i> Paid</button>';
                                    }
                                  }
                                  echo'
                            </td>
                          </tr>
                        ';
                      }
                    }
                    catch(PDOException $e){
                      echo $e->getMessage();
                    }

                    $pdo->close();
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     
  </div>
  	<?php include 'includes/footer.php'; ?>
    <?php include 'includes/orders_modal.php'; ?>

</div>
<!-- ./wrapper -->

<?php include 'includes/scripts.php'; ?>
<script>
$(function(){
	$(document).on('click', '.order_details', function(e){
		e.preventDefault();
		$('#transaction').modal('show');
		var id = $(this).data('id');
		$.ajax({
			type: 'POST',
			url: 'order_details.php',
			data: {id:id},
			dataType: 'json',
			success:function(response){
        $('#id').html(response.id);
        $('#name').html(response.name);
        $('#orders_id_input').val(response.orders_id);
        $('#trans_id_input').val(response.transaction);
        $('#order_type').html(response.order_type);
        $('#address').html(response.address);
        $('#company').html(response.company);
				$('#date').html(response.date);
				$('#transid').html(response.transaction);
        $('#confirmed_by').html(response.confirmed_by);
				$('#detail').prepend(response.list);
				$('#total').html(response.total);
			}
		});
	});

	$("#transaction").on("hidden.bs.modal", function () {
	    $('.prepend_items').remove();
	});
});

$(document).on('click', '.reject', function(e){
  e.preventDefault();
  $('#reject').modal('show');
  var id = $(this).data('id');
  getRow(id);
});

$(document).on('click', '.paid', function(e){
  e.preventDefault();
  $('#paid').modal('show');
  var id = $(this).data('id');
  getRow(id);
});

$(document).on('click', '.delivered', function(e){
  e.preventDefault();
  $('#delivered').modal('show');
  var id = $(this).data('id');
  getRow(id);
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'order_details.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('.orders_id').html(response.orders_id);
      $('.trans_id').html(response.transaction);
      $('#orders_id').val(response.orders_id);
      $('#trans_id').val(response.transaction);
      $('#user_id').val(response.id);
      $('#delivered_orders_id').val(response.orders_id);
      $('#delivered_trans_id').val(response.transaction);
      $('#delivered_user_id').val(response.id);
      $('#paid_order_type').val(response.order_type);
      $('#paid_orders_id').val(response.orders_id);
      $('#paid_trans_id').val(response.transaction);
      $('#paid_user_id').val(response.id);
      $('#paid_email').val(response.user_email);
	  $('#fullname').val(response.name);
    }
  });
}
</script>
</body>
</html>
