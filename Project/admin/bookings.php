<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bookings
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bookings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php
        //PAID
        if(isset($_SESSION['paid'])){
          echo "
            <div class='alert alert-success-book alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success! Booking Service Paid.</h4>
              ".$_SESSION['paid']."
            </div>
          ";
          unset($_SESSION['paid']);
        }
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered" data-order='[[ 0, "desc"]]'>
                <thead>
                  <th>Booking Transaction #</th>
                  <th>Booking Status</th>
                  <th>Services Name</th>
                  <th>Customer Name</th>
                  <th>Address</th>
                  <th>Contact #</th>
                  <th>Booking Date</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                  <?php
                    $conn = $pdo->open();

                    try{
                        $stmt = $conn->prepare("SELECT *, bookings.id AS bookid, bookings.address AS bookaddress FROM bookings LEFT JOIN services ON services.id=bookings.services_id LEFT JOIN users ON users.id=bookings.customer_id ORDER BY bookings.id DESC");
                        $stmt->execute();
                            foreach($stmt as $row){
                            echo "
                                <tr>
                                    <td>".$row['bookingtrans']."</td>
                                    <td>
                                      ";
                                      if($row['booking_status']=='pending'){
                                        echo '<span class="label label-warning">pending</span>  
                                        ';
                                      }elseif ($row['booking_status']=='paid') {
                                        echo '<span class="label label-paid-book">paid</span>';
                                      }elseif ($row['booking_status']=='booked') {
                                        echo '<span class="label label-booked-book">booked</span>';
                                      }elseif ($row['booking_status']=='done') {
                                        echo '<span class="label label-success">done</span>';
                                      }
                                      else{
                                        echo '<span class="label label-danger">rejected</span>';
                                      }
                                        
                                      echo "
                                    </td>
                                    <td>".$row['name']."</td>
                                    <td>
                                    ".$row['firstname'].' '.$row['lastname']."
                                    </td>
                                    <td>".$row['bookaddress']."</td>
                                    <td>".$row['phone']."</td>
                                    <td>".date('M d, Y', strtotime($row['date']))."</td>
                                    <td>";
                                    if($row['booking_status']=='booked'){
                                      echo "
                                      <button class='btn btn-sm btn-flat btn-paid-book paid' data-id='".$row['bookid']."'><i class='fa fa-check'></i> Paid</button>";
                                    }else{
                                      echo "
                                      <button disabled class='btn btn-sm btn-flat btn-disabled'><i class='fa fa-check'></i> Paid</button>";
                                    }
                                    echo "
                                    </td>
                                </tr>
                            ";
                            }
                        }
                    catch(PDOException $e){
                      echo $e->getMessage();
                    }
                    $pdo->close();
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     
  </div>
  	<?php include 'includes/footer.php'; ?>
    <?php include 'includes/bookings_modal.php'; ?>

</div>
<!-- ./wrapper -->

<?php include 'includes/scripts.php'; ?>
<script>
$(document).on('click', '.paid', function(e){
  e.preventDefault();
  $('#paid').modal('show');
  var id = $(this).data('id');
  getRow(id);
});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'booking_details.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('#bookid').val(response.bookid);
      $('#services_id').val(response.services_id);
      $('#booktransaction').val(response.booktransaction);
      $('#fullname').val(response.fullname);
      $('#bookid_show').html(response.bookid);
      $('.booktransaction').html(response.booktransaction);
      $('.fullname').html(response.fullname);
      $('#booking_date').html(response.booking_date);
      $('#service_name').html(response.service_name);
      $('#price').html(response.price);
    }
  });
}

</script>
</body>
</html>
