<div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-calendar"></i> <b>Product Transaction History</b></h4>
</div>
<div class="box-body">
    <table class="table table-bordered" id="example1">
        <thead>
            <th class="hidden"></th>
            <th>Date</th>
            <th>Transaction#</th>
            <th>Amount</th>
            <th>Full Details</th>
        </thead>
    <tbody>
        <?php
            $conn = $pdo->open();

            try{
                $stmt = $conn->prepare("SELECT * FROM orders WHERE user_id=:user_id ORDER BY orders_id DESC");
                $stmt->execute(['user_id'=>$user['id']]);
                foreach($stmt as $row){
                    $stmt2 = $conn->prepare("SELECT * FROM details LEFT JOIN products ON products.id=details.product_id WHERE sales_id=:id");
                    $stmt2->execute(['id'=>$row['orders_id']]);
                    $total = 0;
                    foreach($stmt2 as $row2){
                        $subtotal = $row2['price']*$row2['quantity'];
                        $total += $subtotal;
                    }
                    echo "
                        <tr>
                            <td class='hidden'></td>
                            <td>".date('M d, Y', strtotime($row['order_date']))."</td>
                            <td>".$row['pay_id']."</td>
                            <td>&#8369; ".number_format($total, 2)."</td>
                            <td><button class='btn btn-sm btn-flat btn-info transact' data-id='".$row['orders_id']."'><i class='fa fa-search'></i> View</button></td>
                        </tr>
                    ";
                }

            }
            catch(PDOException $e){
                echo "There is some problem in connection: " . $e->getMessage();
            }

            $pdo->close();
        ?>
        </tbody>
    </table>
</div>