<!-- Edit -->
<div class="modal fade" id="book">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Book this Service?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="booking_service.php">
              <p>
                Customer Name: <span id="fullname"></span>
                <span class="pull-right">Service Name: <span id="service_name"></span></span> 
              </p>
              <p>
                <span class="pull-right">Service Amount: <span id="service_price"></span></span> 
              </p>
              <input type="hidden" class="user_id" name="user_id">
                <input type="hidden" class="firstname" name="firstname">
                <input type="hidden" class="lastname" name="lastname">
                <input type="hidden" class="services_id" name="services_id">

                <div class="row">
                </div>
                <div class="row" style="margin-top: 2rem;">
                  <div class="col-lg-6" style="margin-bottom: 2rem;">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input readonly type="text" class="form-control pull-right col-sm-8" id="reservation" name="booking_date">
                    </div>
                  </div>
                  <div class="col-lg-6" data-tip="Use 09xx format">
                          <label class="sr-only" for="inputPhone">Phone Number</label>
                          <input type="text" class="form-control contact-number" name="contacts" id="inputPhone" placeholder="Contact Number: 09 ..." autocomplete="false" value="<?php echo $user['contact_info']; ?>" required>
                  </div>
                </div>
                <div class="col-lg-12" style="margin: 2rem 0 2rem;">
                  <label class="sr-only" for="inputAddress">Address</label>
                  <input type="text" name="address" class="form-control" id="inputAddress" placeholder="Address" autocomplete="false" value="<?php echo $user['address']; ?>" required>
                </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-booked-book btn-flat" name="book"><i class="fa fa-calendar"></i> Book</button>
              </form>
            </div>
        </div>
    </div>
</div>
