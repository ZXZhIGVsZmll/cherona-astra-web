<div class="text-center mt-5">
    <img src="https://img.icons8.com/dusk/180/000000/shopping-cart.png"/>
    <div class="page-header mt-3">
        <h2>Oops! Your Cart is Currently Empty</h2>
        <p class="lead">Looks like you have no item in your shopping cart.<br>
        Click <a href="index.php">here</a> to continue shopping.
        </p>
    </div>
</div>