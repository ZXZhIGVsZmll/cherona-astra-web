<footer class="main-footer">
	<div class="container">
	  <div class="pull-right hidden-xs">
		<b>All rights reserved</b>
	  </div>
	  <strong>Copyright &copy; 2021 <a href="index.php">Cherona Astra</a> | </strong>
	  <strong>Icons from <a href="https://icons8.com/">icons8.com</a></strong>
	</div>
</footer>