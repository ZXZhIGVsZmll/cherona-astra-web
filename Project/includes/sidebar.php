<div class="row">
	<div class="box box-solid">
	  	<div class="box-header with-border">
	    	<h3 class="box-title"><b>Most Viewed Product Today</b></h3>
	  	</div>
	  	<div class="box-body">
	  		<ul id="trending">
	  		<?php
	  			$now = date('Y-m-d');
	  			$conn = $pdo->open();

	  			$stmt = $conn->prepare("SELECT * FROM products WHERE date_view=:now ORDER BY counter DESC LIMIT 10");
	  			$stmt->execute(['now'=>$now]);
	  			foreach($stmt as $row){
	  				echo "<li><a href='product.php?product=".$row['slug']."'>".$row['name']."</a></li>";
	  			}

	  			$pdo->close();
	  		?>
	    	<ul>
	  	</div>
	</div>
</div>
<div class="row">
	<div class="box box-solid">
	  	<div class="box-header with-border">
	    	<h3 class="box-title"><i class="fa fa-phone"></i><b> Contact Number</b></h3>
	  	</div>
	  	<div class="box-body">
			<p><b>SMART :</b> 09464427336</p>
	    	<p><b>SUN :</b> 09222745259</p>
			<p><b>TM :</b> 09061477511</p>

	  	</div>
	</div>
</div>


<div class="row">
	<div class="box box-solid">
	  	<div class="box-header with-border">
	    	<h3 class="box-title"><b>Become a Subscriber</b></h3>
	  	</div>
	  	<div class="box-body">
	    	<p>Get free updates on the latest products and discounts, straight to your inbox.</p>
	    	<form method="POST" action="">
		    	<div class="input-group">
	                <input type="text" class="form-control" placeholder="Email Address">
	                <span class="input-group-btn">
	                    <button type="button" class="btn btn-info btn-flat"><i class="fa fa-envelope"></i> </button>
	                </span>
	            </div>
		    </form>
	  	</div>
	</div>
</div>

<div class="row">
	<div class='box box-solid'>
	  	<div class='box-header with-border'>
	    	<h3 class='box-title'><b>Follow us on Social Media</b></h3>
	  	</div>
	  	<div class='box-body'>
	    	<a href="https://www.facebook.com/Cherona-Astra-102641508416567/" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
	    	<a href="https://twitter.com/cherona_astra/" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
	    	<a href="https://www.instagram.com/cherona_astra/" class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>	    	
	  	</div>
	</div>
</div>
