<?php
$stmt = $conn->prepare("SELECT * FROM bookings LEFT JOIN services ON services.id=bookings.services_id WHERE bookings.customer_id=:user_id ORDER BY bookings.id DESC LIMIT 1 ");
$stmt->execute(['user_id'=>$_SESSION['user']]);
$booked = $stmt->fetch();
?>
<div class="text-center mt-5">
    <img src="https://img.icons8.com/dusk/180/000000/today.png"/>
    <div class="page-header mt-3">
        <h2>Service Successfully Booked!</h2>
        <p class="lead">Your Order Transaction #: <b><?php echo ''.$booked['bookingtrans'].'';?></b></p>
        <p class="lead">Please follow the instruction below to process your booked service.<br>
        Thank you!
        </p>
    </div>
</div>
<div class="box box-solid">	
    <div class="box-body">
        <div class="bs-callout bs-callout-default">
            <h3>Payment Process</h3>
            Bank of the Philippine Islands (BPI)<br>
            Account NUmber: 1234-5678-90<br>
            Account Name: Cherona Astra<br>
            Account Type: Savings<br>
            Branch: Davao<br>
            - - - <br>
            Banco de Oro (BDO)<br>
            Account NUmber: 1234-5678-90<br>
            Account Name: Cherona Astra<br>
            Account Type: Savings<br>
            Branch: Davao<br>
            <br>
        </div>
        <div class="bs-callout bs-callout-warning">
            <h4>IMPORTANT!</h4><br>
            Please send a snapshot of your deposit slip via<br>
            Email: email@email.com, Messenger: messenger/link,<br>
            or message our live agent here in our page.<br>
            Your ORDER will be processed after this. Thank you!
        </div>
        <div class="bs-callout bs-callout-default">
            <h4><b>Customer Booking Information</b></h4>
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-3"></p>
                    <h4 class="lead">Contact Information</h4>
                    <?php
                    echo '
                    <p class="mb-3">
                    <b>
                    '.$user['email'].'<br>
                    '.$booked['phone'].'
                    </b>
                    </p>
                    ';
                    ?>
                    <h4 class="lead">Service Name</h4>
                    <?php
                        echo '<p><b>'.$booked['name'].'</b></p>';
                    ?>
                </div>
                <div class="col-md-6">
                    <p class="mb-3"></p>
                    <h4 class="lead">Address</h4>
                    <?php
                        echo '<p class="mb-3"><b>'.$booked['address'].'</b></p>';
                    ?>
                </div>
            </div>
        </div>
        <a href="index.php" class="btn btn-primary btn-lg btn-flat">Home</a>
    </div>
</div>