<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Moment JS -->
<script src="bower_components/moment/moment.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- CK Editor -->
<script src="bower_components/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Datatable
    $('#example1').DataTable({
		scrollX: true
	})
    //CK Editor
    CKEDITOR.replace('editor1')
  });
  $(function () {
    // Datatable
    $('#example2').DataTable({
		scrollX: true
	})
    //CK Editor
    CKEDITOR.replace('editor2')
  });
</script>
<!--Magnify -->
<script src="magnify/magnify.min.js"></script>
<script>
$(function(){
	$('.zoom').magnify();
});
</script>
<!-- Custom Scripts -->
<script>
$(function(){
  $('#navbar-search-input').focus(function(){
    $('#searchBtn').show();
  });

  $('#navbar-search-input').focusout(function(){
    $('#searchBtn').hide();
  });

  getCart();

  $('#productForm').submit(function(e){
  	e.preventDefault();
  	var product = $(this).serialize();
  	$.ajax({
  		type: 'POST',
  		url: 'cart_add.php',
  		data: product,
  		dataType: 'json',
  		success: function(response){
  			$('#callout').show();
  			$('.message').html(response.message);
  			if(response.error){
  				$('#callout').removeClass('callout-success').addClass('callout-danger');
  			}
  			else{
				$('#callout').removeClass('callout-danger').addClass('callout-success');
				getCart();
  			}
  		}
  	});
  });

  $(document).on('click', '.close', function(){
  	$('#callout').hide();
  });

});

function getCart(){
	$.ajax({
		type: 'POST',
		url: 'cart_fetch.php',
		dataType: 'json',
		success: function(response){
			$('#cart_menu').html(response.list);
			$('.cart_count').html(response.count);
		}
	});
}



var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();

Tawk_API.visitor = {
	name : '<?php echo "".$user['id']."  ".$user['firstname']." ".$user['lastname'].""; ?>',
	email : '<?php echo $user['email']; ?>'
};

(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/60312c1a9c4f165d47c55498/1ev018iop';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

Tawk_API.onStatusChange = function(status){console.log(status);};
const visitor = {
	name : '<?php echo "".$user['id']."  ".$user['firstname']." ".$user['lastname'].""; ?>',
	email : '<?php echo $user['email']; ?>',
	hash : '<?php echo hash_hmac("sha256", $user['email'], "8d32204baf1d42b89e1cdf4b9db91489e482f5df"); ?>'
	}
Tawk_API.onLoad = function(){
	Tawk_API.setAttributes(visitor);
}

function onlyNumbers(num){
   if ( /[^0-9]+/.test(num.value) ){
      num.value = num.value.replace(/[^0-9]*/g,"")
   }
}

function disableForm(){
	document.getElementById("inputAddress").value = '';
	document.getElementById("inputEtc").value = '';
	document.getElementById("inputPostal").value = '';
	document.getElementById("inputCity").value = '';
	
	document.getElementById("inputAddress").disabled = true;
	document.getElementById("inputEtc").disabled = true;
	document.getElementById("inputPostal").disabled = true;
	document.getElementById("inputCity").disabled = true;
}
function enableForm(){
	document.getElementById("inputAddress").disabled = false;
	document.getElementById("inputEtc").disabled = false;
	document.getElementById("inputPostal").disabled = false;
	document.getElementById("inputCity").disabled = false;
}

</script>