<div class="text-center mt-5">
    <img src="https://img.icons8.com/dusk/180/000000/leave.png"/>
    <div class="page-header mt-3">
        <h2>Oops! No Service Selected</h2>
        <p class="lead">Looks like you need to Select a Service first?<br>
        Click <a href="index.php">here</a> to select a service.
        </p>
    </div>
</div>