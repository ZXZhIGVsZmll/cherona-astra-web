-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 14, 2021 at 06:20 PM
-- Server version: 10.4.14-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u275303287_cherona_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `bookingtrans` varchar(50) NOT NULL,
  `services_id` int(20) NOT NULL,
  `customer_id` int(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `phone` varchar(20) NOT NULL,
  `booking_status` varchar(50) NOT NULL,
  `confirmed_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `bookingtrans`, `services_id`, `customer_id`, `address`, `date`, `phone`, `booking_status`, `confirmed_by`) VALUES
(1, 'BID-WKS7BME2DJQNTI1R80VHG5YX', 4, 27, 'Vista Verde', '2021-03-14', '09123456789', 'paid', 'Doe'),
(2, 'BID-Z8TC523Q9RGAH41E6KIV0DMN', 5, 27, 'Vista Verde', '2021-03-14', '09123456789', 'pending', ''),
(3, 'BID-BX8AEPKYIO6LD73RUJSH24NT', 4, 27, 'Vista Verde', '2021-03-15', '09123456789', 'pending', ''),
(4, 'BID-D5OTU47FVK9SBY2NXJLGP0EC', 4, 27, 'Vista Verde', '2021-03-15', '09123456789', 'pending', '');

-- --------------------------------------------------------

--
-- Table structure for table `booking_sales`
--

CREATE TABLE `booking_sales` (
  `id` int(20) NOT NULL,
  `booking_id` int(20) NOT NULL,
  `sales_date` date NOT NULL,
  `sales_service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_sales`
--

INSERT INTO `booking_sales` (`id`, `booking_id`, `sales_date`, `sales_service_id`) VALUES
(1, 1, '2021-03-14', 4);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `quantity`) VALUES
(2, 39, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `cat_slug` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `cat_slug`) VALUES
(1, 'Laptops', 'laptops'),
(2, 'Desktop PC', 'desktop-pc'),
(5, 'Josh', '');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `sales_id`, `product_id`, `quantity`) VALUES
(1, 1, 1, 1),
(2, 2, 9, 1),
(3, 3, 1, 2),
(4, 4, 11, 3),
(5, 5, 10, 1),
(6, 6, 10, 1),
(7, 7, 4, 1),
(8, 8, 1, 1),
(9, 9, 3, 10),
(10, 10, 2, 1),
(11, 11, 9, 1),
(12, 12, 3, 1),
(13, 13, 9, 1),
(14, 14, 2, 1),
(15, 15, 1, 1),
(16, 16, 3, 1),
(17, 17, 9, 1),
(18, 18, 2, 1),
(19, 19, 9, 1),
(20, 20, 2, 1),
(21, 21, 10, 1),
(22, 22, 8, 1),
(23, 23, 8, 1),
(24, 24, 12, 1),
(25, 25, 2, 1),
(26, 26, 2, 1),
(27, 27, 9, 1),
(28, 28, 2, 1),
(29, 29, 4, 1),
(30, 30, 4, 1),
(31, 31, 9, 1),
(32, 32, 2, 1),
(33, 33, 2, 1),
(34, 34, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orders_id` int(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `pay_id` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `order_type` varchar(50) NOT NULL,
  `contacts` varchar(50) NOT NULL,
  `company` varchar(200) NOT NULL,
  `order_date` date NOT NULL,
  `order_status` varchar(50) NOT NULL,
  `confirmed_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orders_id`, `user_id`, `pay_id`, `address`, `order_type`, `contacts`, `company`, `order_date`, `order_status`, `confirmed_by`) VALUES
(1, '27', 'OID-8HJVO7361KQA2BYFUWP4S9R5', 'Vista Verde Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'delivered', 'Almazan'),
(2, '27', 'OID-IHZTKWRPDB68GUA379NYCVJQ', '', 'pick-up', '09123456789', 'Cherona Astra 2', '2021-03-08', 'paid_pick-up', 'Almazan'),
(3, '29', 'OID-BD7HKFZ35YPOM8XR6W9E1NLG', 'n/a  Davao 8000', 'ship', '09121321323', 'Cherona Astra', '2021-03-08', 'delivered', 'Almazan'),
(4, '29', 'OID-OXYZ5U2BLKITGNVR96CDJS3Q', '', 'pick-up', '09132132123', 'Cherona Astra', '2021-03-08', 'paid_pick-up', 'Almazan'),
(5, '29', 'OID-7AJZ5QLSHEMFGD9KP1NIT4O2', '', 'pick-up', '09123456788', '', '2021-03-08', 'pending', ''),
(6, '29', 'OID-CMYWZKEVQ5A4L9SBJDNUOPIT', 'n/a Apt 5 Davao 8000', 'ship', '09123456788', 'Cherona Astra', '2021-03-08', 'pending', ''),
(7, '29', 'OID-6E8MUFSOT9L1H2PNYB5DCJ7R', '', 'pick-up', '09123456788', '', '2021-03-08', 'pending', ''),
(8, '29', 'OID-IAS7ZQEJFUC6RP8WML4YTV9X', 'Vista Verde Apt 5 Davao 8000', 'ship', '09123456788', 'Cherona Astra', '2021-03-08', 'pending', ''),
(9, '29', 'OID-WLHGYTA5X4QNDJB37MPCF9U6', '', 'pick-up', '09123456788', '', '2021-03-08', 'pending', ''),
(10, '29', 'OID-Y2CSG3LKU18ORPFT47WHZX5M', '', 'pick-up', '09123456788', '', '2021-03-08', 'pending', ''),
(11, '29', 'OID-23G4RFHT7OXIPNWV6BQDKCZ9', 'n/a Apt 5 Davao 8000', 'ship', '09123456788', 'Cherona Astra', '2021-03-08', 'pending', ''),
(12, '29', 'OID-P6R4CJQZEK8OF2TI3GU1BS5M', 'n/a Apt 1 Davao 8000', 'ship', '09123456788', 'Cherona Astra', '2021-03-08', 'pending', ''),
(13, '29', 'OID-E4HNYT79OGQJDZ621KX5IAU3', '', 'pick-up', '09123456788', '', '2021-03-08', 'pending', ''),
(14, '29', 'OID-AN2GRTBJQ98OYXKMDWPSL6HE', '', 'pick-up', '09123456788', '', '2021-03-08', 'pending', ''),
(15, '29', 'OID-ME2PX4N69FH87SC3VORQ1I5D', 'n/a Apt 1 Davao 8000', 'ship', '09123456788', 'Cherona Astra', '2021-03-08', 'reject', 'Doe'),
(16, '29', 'OID-63T8L21R5E4IQMZFXYD9GBSJ', 'n/a Apt 1 Davao 8000', 'ship', '09123456788', 'Cherona Astra', '2021-03-08', 'delivered', 'Doe'),
(17, '29', 'OID-5PUVO7DIY3SXMKTAC8RGJ9N4', 'n/a Apt 1 Davao 8000', 'ship', '09123456788', 'q weqwesd sdsaasdsadsad', '2021-03-08', 'reject', 'Doe'),
(18, '27', 'OID-53TM6CEQNVAZKFYPW4UO9SBD', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'paid', 'Doe'),
(19, '27', 'OID-L6P8HCRKA4G73ENQYZD5TVSO', '', 'pick-up', '09123456789', '', '2021-03-08', 'paid_pick-up', 'Doe'),
(20, '27', 'OID-4B3UVCHPOKMZ6JRF8N7D1LAT', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'delivered', 'Doe'),
(21, '27', 'OID-ZJ7K463GOF5IAXLUQVSMCD2Y', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'reject', 'Almazan'),
(22, '27', 'OID-6V8PTKSM49BW5OL1DXNJCUGH', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'reject', 'Almazan'),
(23, '27', 'OID-7ORWY1KGX3U4DB58ZLHPVQ9J', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'paid', 'Almazan'),
(24, '27', 'OID-WHVC4QA9UOXTE6RP8F52ISZK', '', 'pick-up', '09123456789', '', '2021-03-08', 'paid_pick-up', 'Doe'),
(25, '27', 'OID-AUZGBPDI6YFJOH547EMQRTXL', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'delivered', 'Almazan'),
(26, '27', 'OID-WSH3429YZPXVJCNMRUL6O7DQ', '', 'pick-up', '09123456789', '', '2021-03-08', 'paid_pick-up', 'Almazan'),
(27, '27', 'OID-O4EKGLMC2SNTU8W6I71FDYBQ', '', 'pick-up', '09123456789', '', '2021-03-08', 'paid_pick-up', 'Almazan'),
(28, '27', 'OID-4XGEK68M5SQZ1OHDVBFINWU7', '', 'pick-up', '09123456789', '', '2021-03-08', 'reject', 'Almazan'),
(29, '27', 'OID-L36EOQDSBT5WIUJG8RAHZCFK', '', 'pick-up', '09123456789', '', '2021-03-08', 'paid_pick-up', 'Almazan'),
(30, '27', 'OID-1DZYL5FAQO82R7XNIBWGKJT9', 'n/a Apt 1 Davao 8000', 'ship', '09123456789', 'Cherona Astra', '2021-03-08', 'reject', 'Almazan'),
(31, '27', 'OID-L3O5PNM6Q9BWTGFYSUHXIARC', '', 'pick-up', '09123456789', '', '2021-03-12', 'pending', ''),
(32, '27', 'OID-VFOL9IJANUSMEGX7HWTD32ZK', '', 'pick-up', '09123456789', '', '2021-03-12', 'pending', ''),
(33, '27', 'OID-QI9Y25ED1TX7HKMCZ8PARBGL', '', 'pick-up', '09123456789', '', '2021-03-14', 'pending', ''),
(34, '27', 'OID-3VR7YANUCHSO2ZIW61MBPJKE', '', 'pick-up', '09123456789', '', '2021-03-14', 'pending', '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `photo` varchar(200) NOT NULL,
  `date_view` date NOT NULL,
  `counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `slug`, `price`, `photo`, `date_view`, `counter`) VALUES
(1, 1, 'DELL Inspiron 15 7000 15.6', '<p>15-inch laptop ideal for gamers. Featuring the latest Intel&reg; processors for superior gaming performance, and life-like NVIDIA&reg; GeForce&reg; graphics and an advanced thermal cooling design.</p>\r\n', 'dell-inspiron-15-7000-15-6', 899, 'dell-inspiron-15-7000-15-6.jpg', '2021-03-14', 2),
(2, 1, 'MICROSOFT Surface Pro 4 & Typecover - 128 GB', '<p>Surface Pro 4 powers through everything you need to do, while being lighter than ever before</p>\r\n\r\n<p>The 12.3 PixelSense screen has extremely high contrast and low glare so you can work through the day without straining your eyes</p>\r\n\r\n<p>keyboard is not included and needed to be purchased separately</p>\r\n\r\n<p>Features an Intel Core i5 6th Gen (Skylake) Core,Wireless: 802.11ac Wi-Fi wireless networking; IEEE 802.11a/b/g/n compatible Bluetooth 4.0 wireless technology</p>\r\n\r\n<p>Ships in Consumer packaging.</p>\r\n', 'microsoft-surface-pro-4-typecover-128-gb', 799, 'microsoft-surface-pro-4-typecover-128-gb.jpg', '2021-03-14', 2),
(3, 1, 'DELL Inspiron 15 5000 15.6', '<p>Dell&#39;s 15.6-inch, midrange notebook is a bland, chunky block. It has long been the case that the Inspiron lineup lacks any sort of aesthetic muse, and the Inspiron 15 5000 follows this trend. It&#39;s a plastic, silver slab bearing Dell&#39;s logo in a mirror sheen.</p>\r\n\r\n<p>Lifting the lid reveals the 15.6-inch, 1080p screen surrounded by an almost offensively thick bezel and a plastic deck with a faux brushed-metal look. There&#39;s a fingerprint reader on the power button, and the keyboard is a black collection of island-style keys.</p>\r\n', 'dell-inspiron-15-5000-15-6', 599, 'dell-inspiron-15-5000-15-6.jpg', '2021-03-14', 2),
(4, 1, 'LENOVO Ideapad 320s-14IKB 14\" Laptop - Grey', '<p>- Made for portability with a slim, lightweight chassis design&nbsp;<br />\r\n<br />\r\n- Powerful processing helps you create and produce on the go&nbsp;<br />\r\n<br />\r\n- Full HD screen ensures crisp visuals for movies, web pages, photos and more&nbsp;<br />\r\n<br />\r\n- Enjoy warm, sparkling sound courtesy of two Harman speakers and Dolby Audio&nbsp;<br />\r\n<br />\r\n- Fast data transfer and high-quality video connection with USB-C and HDMI ports&nbsp;<br />\r\n<br />\r\nThe Lenovo&nbsp;<strong>IdeaPad 320s-14IKB 14&quot; Laptop</strong>&nbsp;is part of our Achieve range, which has the latest tech to help you develop your ideas and work at your best. It&#39;s great for editing complex documents, business use, editing photos, and anything else you do throughout the day.</p>\r\n', 'lenovo-ideapad-320s-14ikb-14-laptop-grey', 399, 'lenovo-ideapad-320s-14ikb-14-laptop-grey.jpg', '2021-03-13', 12),
(5, 3, 'APPLE 9.7\" iPad - 32 GB, Gold', '<p>9.7 inch Retina Display, 2048 x 1536 Resolution, Wide Color and True Tone Display</p>\r\n\r\n<p>Apple iOS 9, A9X chip with 64bit architecture, M9 coprocessor</p>\r\n\r\n<p>12 MP iSight Camera, True Tone Flash, Panorama (up to 63MP), Four-Speaker Audio</p>\r\n\r\n<p>Up to 10 Hours of Battery Life</p>\r\n\r\n<p>iPad Pro Supports Apple Smart Keyboard and Apple Pencil</p>\r\n', 'apple-9-7-ipad-32-gb-gold', 339, 'apple-9-7-ipad-32-gb-gold.jpg', '2018-05-10', 3),
(6, 1, 'DELL Inspiron 15 5000 15', '<p>15-inch laptop delivering an exceptional viewing experience, a head-turning finish and an array of options designed to elevate your entertainment, wherever you go.</p>\r\n', 'dell-inspiron-15-5000-15', 449.99, 'dell-inspiron-15-5000-15.jpg', '2021-02-23', 4),
(7, 3, 'APPLE 10.5\" iPad Pro - 64 GB, Space Grey (2017)', '<p>4K video recording at 30 fps</p>\r\n\r\n<p>12-megapixel camera</p>\r\n\r\n<p>Fingerprint resistant coating</p>\r\n\r\n<p>Antireflective coating</p>\r\n\r\n<p>Face Time video calling</p>\r\n', 'apple-10-5-ipad-pro-64-gb-space-grey-2017', 619, 'apple-10-5-ipad-pro-64-gb-space-grey-2017.jpg', '0000-00-00', 0),
(8, 1, 'ASUS Transformer Mini T102HA 10.1\" 2 in 1 - Silver', '<p>Versatile Windows 10 device with keyboard and pen included, 2-in-1 functionality: use as both laptop and tablet.Update Windows periodically to ensure that your applications have the latest security settings.</p>\r\n\r\n<p>All day battery life, rated up to 11 hours of video playback; 128GB Solid State storage. Polymer Battery.With up to 11 hours between charges, you can be sure that Transformer Mini T102HA will be right there whenever you need it. You can charge T102HA via its micro USB port, so you can use a mobile charger or any power bank with a micro USB connector.</p>\r\n', 'asus-transformer-mini-t102ha-10-1-2-1-silver', 549.99, 'asus-transformer-mini-t102ha-10-1-2-1-silver.jpg', '2021-03-08', 3),
(9, 2, 'PC SPECIALIST Vortex Core Lite Gaming PC', '<p>- Top performance for playing eSports and more&nbsp;<br />\r\n<br />\r\n- NVIDIA GeForce GTX graphics deliver smooth visuals&nbsp;<br />\r\n<br />\r\n- GeForce Experience delivers updates straight to your PC&nbsp;<br />\r\n<br />\r\nThe PC Specialist&nbsp;<strong>Vortex Core Lite&nbsp;</strong>is part of our Gaming range, bringing you the most impressive PCs available today. It has spectacular graphics and fast processing performance to suit the most exacting players.</p>\r\n', 'pc-specialist-vortex-core-lite-gaming-pc', 599.99, 'pc-specialist-vortex-core-lite-gaming-pc.jpg', '2021-03-12', 1),
(10, 2, 'DELL Inspiron 5675 Gaming PC - Recon Blue', '<p>All-new gaming desktop featuring powerful AMD Ryzen&trade; processors, graphics ready for VR, LED lighting and a meticulous design for optimal cooling.</p>\r\n', 'dell-inspiron-5675-gaming-pc-recon-blue', 599.99, 'dell-inspiron-5675-gaming-pc-recon-blue.jpg', '2021-03-13', 1),
(11, 2, 'HP Barebones OMEN X 900-099nn Gaming PC', '<p>What&#39;s inside matters.</p>\r\n\r\n<p>Without proper cooling, top tierperformance never reaches its fullpotential.</p>\r\n\r\n<p>Nine lighting zones accentuate theaggressive lines and smooth blackfinish of this unique galvanized steelcase.</p>\r\n', 'hp-barebones-omen-x-900-099nn-gaming-pc', 489.98, 'hp-barebones-omen-x-900-099nn-gaming-pc.jpg', '2021-03-08', 2),
(12, 2, 'ACER Aspire GX-781 Gaming PC', '<p>- GTX 1050 graphics card lets you play huge games in great resolutions&nbsp;<br />\r\n<br />\r\n- Latest generation Core&trade; i5 processor can handle demanding media software&nbsp;<br />\r\n<br />\r\n- Superfast SSD storage lets you load programs in no time&nbsp;<br />\r\n<br />\r\nThe Acer&nbsp;<strong>Aspire&nbsp;GX-781 Gaming PC&nbsp;</strong>is part of our Gaming range, which offers the most powerful PCs available today. It has outstanding graphics and processing performance to suit the most demanding gamer.</p>\r\n', 'acer-aspire-gx-781-gaming-pc', 750, 'acer-aspire-gx-781-gaming-pc.jpg', '2021-03-08', 1),
(13, 2, 'HP Pavilion Power 580-015na Gaming PC', '<p>Features the latest quad core Intel i5 processor and discrete graphics. With this power, you&rsquo;re ready to take on any activity from making digital art to conquering new worlds in VR.</p>\r\n\r\n<p>Choose the performance and storage you need. Boot up in seconds with to 128 GB SSD.</p>\r\n\r\n<p>Ditch the dull grey box, this desktop comes infused with style. A new angular bezel and bold green and black design give your workspace just the right amount of attitude.</p>\r\n\r\n<p>Up to 3 times faster performance - GeForce GTX 10-series graphics cards are powered by Pascal to deliver twice the performance of previous-generation graphics cards.</p>\r\n', 'hp-pavilion-power-580-015na-gaming-pc', 799.99, 'hp-pavilion-power-580-015na-gaming-pc.jpg', '2021-03-14', 1),
(14, 2, 'LENOVO Legion Y520 Gaming PC', '<p>- Multi-task with ease thanks to Intel&reg; i5 processor&nbsp;<br />\r\n<br />\r\n- Prepare for battle with NVIDIA GeForce GTX graphics card&nbsp;<br />\r\n<br />\r\n- VR ready for the next-generation of immersive gaming and entertainment<br />\r\n<br />\r\n- Tool-less upgrade helps you personalise your system to your own demands&nbsp;<br />\r\n<br />\r\nPart of our Gaming range, which features the most powerful PCs available today, the Lenovo&nbsp;<strong>Legion Y520 Gaming PC</strong>&nbsp;has superior graphics and processing performance to suit the most demanding gamer.</p>\r\n', 'lenovo-legion-y520-gaming-pc', 899.99, 'lenovo-legion-y520-gaming-pc.jpg', '2021-02-24', 3),
(15, 2, 'PC SPECIALIST Vortex Minerva XT-R Gaming PC', '<p>- NVIDIA GeForce GTX graphics for stunning gaming visuals<br />\r\n<br />\r\n- Made for eSports with a speedy 7th generation Intel&reg; Core&trade; i5 processor<br />\r\n<br />\r\n- GeForce technology lets you directly update drivers, record your gaming and more<br />\r\n<br />\r\nThe PC Specialist&nbsp;<strong>Vortex Minerva XT-R Gaming PC</strong>&nbsp;is part of our Gaming range, which offers the most powerful PCs available. Its high-performance graphics and processing are made to meet the needs of serious gamers.</p>\r\n', 'pc-specialist-vortex-minerva-xt-r-gaming-pc', 999.99, 'pc-specialist-vortex-minerva-xt-r-gaming-pc.jpg', '2021-03-08', 1),
(16, 2, 'PC SPECIALIST Vortex Core II Gaming PC', '<p>Processor: Intel&reg; CoreTM i3-6100 Processor- Dual-core- 3.7 GHz- 3 MB cache</p>\r\n\r\n<p>Memory (RAM): 8 GB DDR4 HyperX, Storage: 1 TB HDD, 7200 rpm</p>\r\n\r\n<p>Graphics card: NVIDIA GeForce GTX 950 (2 GB GDDR5), Motherboard: ASUS H110M-R</p>\r\n\r\n<p>USB: USB 3.0 x 3- USB 2.0 x 5, Video interface: HDMI x 1- DisplayPort x 1- DVI x 2, Audio interface: 3.5 mm jack, Optical disc drive: DVD/RW, Expansion card slot PCIe: (x1) x 2</p>\r\n\r\n<p>Sound: 5.1 Surround Sound support PSU Corsair: VS350, 350W, Colour: Black- Green highlights, Box contents: PC Specialist Vortex Core- AC power cable</p>\r\n', 'pc-specialist-vortex-core-ii-gaming-pc', 649.99, 'pc-specialist-vortex-core-ii-gaming-pc.jpg', '2021-02-24', 1),
(17, 3, 'AMAZON Fire 7 Tablet with Alexa (2017) - 8 GB, Black', '<p>The next generation of our best-selling Fire tablet ever - now thinner, lighter, and with longer battery life and an improved display. More durable than the latest iPad</p>\r\n\r\n<p>Beautiful 7&quot; IPS display with higher contrast and sharper text, a 1.3 GHz quad-core processor, and up to 8 hours of battery life. 8 or 16 GB of internal storage and a microSD slot for up to 256 GB of expandable storage.</p>\r\n', 'amazon-fire-7-tablet-alexa-2017-8-gb-black', 49.99, 'amazon-fire-7-tablet-alexa-2017-8-gb-black.jpg', '2018-05-12', 1),
(18, 3, 'AMAZON Fire HD 8 Tablet with Alexa (2017) - 16 GB, Black', '<p>Take your personal assistant with you wherever you go with this Amazon Fire HD 8 tablet featuring Alexa voice-activated cloud service. The slim design of the tablet is easy to handle, and the ample 8-inch screen is ideal for work or play. This Amazon Fire HD 8 features super-sharp high-definition graphics for immersive streaming.</p>\r\n', 'amazon-fire-hd-8-tablet-alexa-2017-16-gb-black', 79.99, 'amazon-fire-hd-8-tablet-alexa-2017-16-gb-black.jpg', '2018-05-12', 2),
(19, 3, 'AMAZON Fire HD 8 Tablet with Alexa (2017) - 32 GB, Black', '<p>The next generation of our best-reviewed Fire tablet, with up to 12 hours of battery life, a vibrant 8&quot; HD display, a 1.3 GHz quad-core processor, 1.5 GB of RAM, and Dolby Audio. More durable than the latest iPad.</p>\r\n\r\n<p>16 or 32 GB of internal storage and a microSD slot for up to 256 GB of expandable storage.</p>\r\n', 'amazon-fire-hd-8-tablet-alexa-2017-32-gb-black', 99.99, 'amazon-fire-hd-8-tablet-alexa-2017-32-gb-black.jpg', '2018-05-10', 1),
(20, 3, 'APPLE 9.7\" iPad - 32 GB, Space Grey', '<p>9.7-inch Retina display, wide color and true tone</p>\r\n\r\n<p>A9 third-generation chip with 64-bit architecture</p>\r\n\r\n<p>M9 motion coprocessor</p>\r\n\r\n<p>1.2MP FaceTime HD camera</p>\r\n\r\n<p>8MP iSight camera</p>\r\n\r\n<p>Touch ID</p>\r\n\r\n<p>Apple Pay</p>\r\n', 'apple-9-7-ipad-32-gb-space-grey', 339, 'apple-9-7-ipad-32-gb-space-grey.jpg', '2018-05-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `orders_id` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pay_id` varchar(50) NOT NULL,
  `sales_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `orders_id`, `user_id`, `pay_id`, `sales_date`) VALUES
(1, 4, 29, 'OID-OXYZ5U2BLKITGNVR96CDJS3Q', '2021-03-08'),
(2, 3, 29, 'OID-BD7HKFZ35YPOM8XR6W9E1NLG', '2021-03-08'),
(3, 2, 27, 'OID-IHZTKWRPDB68GUA379NYCVJQ', '2021-03-08'),
(4, 1, 27, 'OID-8HJVO7361KQA2BYFUWP4S9R5', '2021-03-08'),
(5, 29, 27, 'OID-L36EOQDSBT5WIUJG8RAHZCFK', '2021-03-08'),
(6, 25, 27, 'OID-AUZGBPDI6YFJOH547EMQRTXL', '2021-03-08'),
(7, 27, 27, 'OID-O4EKGLMC2SNTU8W6I71FDYBQ', '2021-03-09'),
(8, 26, 27, 'OID-WSH3429YZPXVJCNMRUL6O7DQ', '2021-03-09'),
(9, 19, 27, 'OID-L6P8HCRKA4G73ENQYZD5TVSO', '2021-03-10'),
(10, 20, 27, 'OID-4B3UVCHPOKMZ6JRF8N7D1LAT', '2021-03-10'),
(11, 24, 27, 'OID-WHVC4QA9UOXTE6RP8F52ISZK', '2021-03-10'),
(12, 16, 29, 'OID-63T8L21R5E4IQMZFXYD9GBSJ', '2021-03-10');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `service_admin_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `photo` varchar(200) NOT NULL,
  `date_view` datetime NOT NULL,
  `counter` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `category_id`, `service_admin_id`, `name`, `description`, `slug`, `price`, `photo`, `date_view`, `counter`) VALUES
(1, 3, 33, 'Bob the Builder', '<p>Imong mama bob the builder</p>\r\n', 'bob-builder', 500, 'bob-builder.jpg', '2021-03-11 00:00:00', 1),
(2, 1, 32, 'Cherona Web Astra', '<p>CLEAN TAG IRCON MGA PRE!</p>\r\n', 'cherona-web-astra', 500, 'cherona-web-astra.png', '2021-03-13 00:00:00', 1),
(4, 1, 31, 'ABC Home Designers', '<p>aw</p>\r\n', 'abc-home-designers', 1500, 'abc-home-designers_1615308964.jpg', '2021-03-14 00:00:00', 1),
(5, 4, 36, 'ABC Graphic Designer Company', '<p>We value our Customer. We deliver quality designs!</p>\r\n', 'abc-graphic-designer-company', 1500, 'abc-graphic-designer-company.jpg', '2021-03-13 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_category`
--

CREATE TABLE `service_category` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `cat_slug` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_category`
--

INSERT INTO `service_category` (`id`, `name`, `cat_slug`) VALUES
(1, 'Aircon Cleaning', 'aircon-cleaning'),
(3, 'Online Teacher', 'online-teacher'),
(4, 'Graphic Designer', 'graphic-designer');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(60) NOT NULL,
  `type` int(1) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `contact_info` varchar(100) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `status` int(1) NOT NULL,
  `activate_code` varchar(15) NOT NULL,
  `reset_code` varchar(15) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `type`, `firstname`, `lastname`, `address`, `contact_info`, `photo`, `status`, `activate_code`, `reset_code`, `created_on`) VALUES
(1, 'admin@admin.com', '$2y$10$8wY63GX/y9Bq780GBMpxCesV9n1H6WyCqcA2hNy2uhC59hEnOpNaS', 1, 'Josh', 'Almazan', '', '', 'profile youtube1.jpg', 1, '', '', '2018-05-01'),
(14, 'josh@gmail.com', '$2y$10$tNLPM1DyAvpF/Yobu8Z42O/Vx0ev1h/7NLcDoEE1lxknFD4pHEVL6', 0, 'josh', 'Almazan', 'bajada', '09129315081', 'p.jpg', 1, '', '', '2021-01-24'),
(15, 'email@email.co', '$2y$10$PonjIYACol6KlArhPjxdN.3rM19SGPJm7a.uBRKFOQ8K73nvYE5dq', 0, 'John', 'Doe', 'Ph', '123', '', 1, '', '', '2021-01-24'),
(16, 'john@email.com', '$2y$10$LhQzdKHVqO8AtzFylwu.aeETxKcoUuHj36DwxPmeQY9ac4bkDtBNC', 0, 'John', 'Doe', '', '', '', 1, 'lbWywz4f2cnU', '', '2021-01-26'),
(25, 'janedoe123@gmail.com', '$2y$10$RWHkzI5rlmsjoOPdxxAkVuc6bxnTNmYjXUy7fz7Qc4klWWRXZKzN6', 0, 'Jane', 'Doe', '', '', '', 0, 'hH5PpZSLwJfz', '', '2021-01-28'),
(27, 'qwe123@qwe.com', '$2y$10$Xw4Qaz5Q84ij2hqg2DT.5e7RNwjzJFdzLIb7Hv4OFjJjuhhVdnViO', 0, 'Jane', 'Doe', 'Vista Verde', '09123456789', '', 1, 'LafpoiTDnvRz', '', '2021-02-04'),
(28, 'jd@email.com', '$2y$10$jgsQVDgvVsrze5Kzp1g3I.nJmCFx0aOo9fFUFu0tOWhWyHADKzxU6', 1, 'John', 'Doe', '', '', '', 1, 'BNnxA5aVUtWY', '', '2021-02-10'),
(29, 'qwe1234@qwe.com', '$2y$10$PysBp.kJanGvkcU9gg30Y.fs/blEcXLloevltB1Avc45Fp6UAT7cO', 0, 'John', 'Doe', '', '', '', 1, 'xo73HMXpbKQk', '', '2021-02-16'),
(30, 'admin123@admin.com', '$2y$10$lJKXVBawZ5WuB2rvO4eiy.J9uQ5aN40yVLwL0GY2bA37RPVUJ.u8i', 1, 'John', 'Doe', '', '', '', 1, 'Uhk3dZLFtf9i', '', '2021-02-16'),
(31, 'email123@email.com', '$2y$10$jAhgi1BhYBZWKQa8Tkmkbe7Al5qiGi3b39jit0g3u0bbUOJqO5o/a', 2, 'John', 'Doe', 'Vista Verde Davao', '09123456789', '', 1, '', '', '2021-03-07'),
(32, 'robertducusin@gmail.com', '$2y$10$TBCwqgrCnwjswb8c9hNAy.sjS7hPEuQzc2NQckRx65vEQxhBL6vva', 2, 'Robert Victor', 'Ducusin', 'Panacan Davao', '09123456789', 'sale.png', 1, '', '', '2021-03-09'),
(33, 'email@email.com', '$2y$10$r4I0V4Y7WjurxjmlG43lLO20EjmGFiPBLrMmFKvpus/qhz0i1cCwW', 2, 'Robert', 'Ducusin', 'n/a\r\nApt 1', '09123456789', '', 1, '', '', '2021-03-09'),
(34, 'johnnysins@email.com', '$2y$10$t8iVEYAUFxIH4GVTHt9ib.k2rictOGHrYEUJXDcbbNo4sy.P73xXu', 2, 'Johnny', 'Sins', 'USA America', '09123456789', '', 1, '', '', '2021-03-09'),
(35, 'alexadads@email.com', '$2y$10$3skBhk91erOQhvdMaDFnEu/Q.6SdUF82nSwhkWej6LO5mHuuvMR0m', 2, 'Alexandra', 'Daddario', 'New York, New York City', '09123456789', '', 1, '', '', '2021-03-09'),
(36, 'asakira@email.com', '$2y$10$FSjV6eqJhluSUsdWWYxSD.x1g/0bSydUPQM9rhirsoCpaTSCOOIMK', 2, 'Asa', 'Akira', 'USA America', '09123456789', '', 1, '', '', '2021-03-09'),
(37, 'evaelfie@email.com', '$2y$10$vjDxPZZmDqBkYyLqMoHu7eaHPyeDWEW9TzfSTDE7Vsk3bYmRnEw.O', 0, 'Eva', 'Elfie', '', '', '', 1, 'K7RaIFNxd8MA', '', '2021-03-11'),
(38, 'email1234@email.com', '$2y$10$nHokWJtZMGORtfh0xJd/ruaSV74JCnjIzVjeC6yhwHas146UcxHXq', 0, 'Jessice', 'Sanchez', '', '', '', 1, 'v9GD7quoWO5x', '', '2021-03-11'),
(39, 'joshalmazan@gmail.com', '$2y$10$YxWQhF7NHNx7GEemtGxMvOTfpqLnUAjdKgYlFUXxX/YiqsOPzaloW', 0, 'josh', 'Almazan', '', '', '', 1, 'xD1M5qEA6hSQ', '', '2021-03-13'),
(40, 'joshserv@gmail.com', '$2y$10$lCry3tlBgjLT01m8ntvYq.VZhFz6f2FgGAMqq.aKLts63EcHi5RvO', 2, 'Josh', 'Almazan', 'Bajada', '09932424344', '16157105297244749100682964543806.jpg', 1, '', '', '2021-03-14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_sales`
--
ALTER TABLE `booking_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orders_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_category`
--
ALTER TABLE `service_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `booking_sales`
--
ALTER TABLE `booking_sales`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orders_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `service_category`
--
ALTER TABLE `service_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
