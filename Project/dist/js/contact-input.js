$(document).ready(function(){
	// Contact number attributes
	$(".contact-number").attr('maxlength', 11);
	$(".contact-number").attr('pattern', "^09\\d{9}$");
	$(".contact-number").attr('title', "Use 09xx format");
	// Contact number filter
	(function($){
		$.fn.inputFilter = function(inputFilter) {
			return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function(){
				if (inputFilter(this.value)){
					this.oldValue = this.value;
					this.oldSelectionStart = this.selectionStart;
					this.oldSelectionEnd = this.selectionEnd;
				} else if (this.hasOwnProperty("oldValue")){
					this.value = this.oldValue;
					this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
				}
			});
		};
	}(jQuery));
	$(".contact-number").inputFilter(function(value){
		return /^\d*$/.test(value);
	});
});
