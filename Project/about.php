<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<?php include 'includes/unset_sessions.php'; ?>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

	<?php include 'includes/navbar.php'; ?>
	 
	  <div class="content-wrapper">
	    <div class="container">

	      <!-- Main content -->
	      <section class="content">
	        <div class="row">
	        	<div class="col-sm-9">
<style>
.card-img {
    width: 100%;
    height: 32vw;
 
}
</style>
<div class="card  card-body ">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="img/logo1.jpg" class="card-img" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title"><b>ABOUT US</b></h5>
<br>
<br>
<br>
        <p class="card-text"> Cherona Astra offers a variety of products from different brands and allows clients to conveniently connect with different service providers they are looking for. Its desire is to distribute products at an affordable and reasonable price.  It also aims to support local service providers by giving them a platform that will showcase their services to  potential clients while protecting both of them from fraud.
.</p>
      </div>
    </div>
  </div>
</div>
  			  
	        	</div>
	        	<div class="col-sm-3">
	        		<?php include 'includes/sidebar.php'; ?>
	        	</div>
	        </div>
	      </section>
		</div>
	    </div>
	  </div>

  	<?php include 'includes/footer.php'; ?>
</div>

<?php include 'includes/scripts.php'; ?>
</body>
</html>