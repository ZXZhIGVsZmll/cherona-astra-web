<?php
include 'includes/session.php';
if(isset($_SESSION['user'])){
header('location: cart_view.php');
}
if(isset($_GET['redir'])) {
	$redir = htmlspecialchars($_GET['redir']);
}elseif(!isset($_GET['redir'])) {
	$redir = "profile.php";
}
?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">
		<?php include 'includes/navbar.php'; ?>
		<div class="content-wrapper">
			<div class="container">
				<section class="content">
					<div class="row">
						<div class="col col-md-6 col-md-offset-3">
							<div class="panel panel-default" style="margin:5%;">
								<?php
									if(isset($_SESSION['error'])){
										echo "
											<div class='callout callout-danger text-center'>
											<p>".$_SESSION['error']."</p> 
											</div>
										";
									unset($_SESSION['error']);
									}
									if(isset($_SESSION['success'])){
										echo "
											<div class='callout callout-success text-center'>
											<p>".$_SESSION['success']."</p> 
											</div>
										";
									unset($_SESSION['success']);
									}
								?>
								<div class="panel-body">
									<p class="login-box-msg">Sign in</p>
									<form action="verify.php" method="POST">
										<input type="hidden" name="redir" value="<?php echo $redir;?>">
										<div class="form-group has-feedback">
											<input type="email" class="form-control" name="email" placeholder="Email" autofocus required>
											<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
										</div>
									  <div class="form-group has-feedback">
										<input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
										<span class="glyphicon glyphicon-lock form-control-feedback"></span>
										<div id="showPass" class="text-right no-select">
											<i id="eye" class="fa fa-eye"></i>
											<span id="password-text">Show Password</span>
										</div>
									  </div>
										<div class="row">
											<div class="col-xs-4 col-xs-offset-4">
												<button type="submit" class="btn btn-primary btn-block" name="login"><i class="fa fa-sign-in"></i> Sign In</button>
											</div>
										</div>
									</form>
									<br>
									<div class="text-center">
										<p>Not yet registered? <a href="signup.php">Register now</a></p>
										<a href="password_forgot.php">Forgot your password?</a><br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php include 'includes/footer.php'; ?>
	</div>
	<?php include 'includes/scripts.php' ?>
	<script type="text/javascript">
		// Toggle password visibility
		$(document).ready(function(){
			var pwdtxt = $("#password-text");
			var showPass = $("#showPass");
			var password = $("#password");
			var eye = $("#eye");
			showPass.click(function(){
				if(password.is(':password')) {
					password.attr('type', 'text');
					eye.attr('class', 'fa fa-eye-slash');
					pwdtxt.text("Hide Password");
				} else if (password.is(':text')) {
					password.attr('type', 'password');
					eye.attr('class', 'fa fa-eye');
					pwdtxt.text("Show Password");
				}
			})
		});
	</script>
</body>
</html>
