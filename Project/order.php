<?php
	include 'includes/session.php';

	if(isset($_POST['checkout'])){
		$date = date('Y-m-d');

		//generate code
		$set='123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$temp_pay=substr(str_shuffle($set), 0, 24);
		$payid = "OID-$temp_pay";
		$orderType = strval($_POST['radio']);
		$company = $_POST['company'];
		$address = $_POST['address'] . ' ' . $_POST['addExt'] . ' ' . $_POST['city'] . ' ' . $_POST['postal'];
		$contacts = $_POST['contacts'];
		$order_status = "pending";

		$conn = $pdo->open();
        $stmt = $conn->prepare("SELECT COUNT(*) AS numrows FROM cart WHERE user_id=:user_id");
        $stmt->execute(['user_id'=>$user['id']]);
        $row = $stmt->fetch();

        if($orderType=="pick-up"){
            try {
                if($row['numrows'] > 0){
                    try{
                        $stmt = $conn->prepare("INSERT INTO orders (user_id, pay_id, order_type, contacts, company, order_date, order_status) VALUES (:user_id, :pay_id, :order_type, :contacts, :company, :order_date, :order_status)");
                        $stmt->execute(['user_id'=>$user['id'], 'pay_id'=>$payid, 'order_type'=>$orderType, 'contacts'=>$contacts, 'company'=>$company, 'order_date'=>$date, 'order_status'=>$order_status]);
                        $salesid = $conn->lastInsertId();
                        
                        try{
                            $stmt = $conn->prepare("SELECT * FROM cart LEFT JOIN products ON products.id=cart.product_id WHERE user_id=:user_id");
                            $stmt->execute(['user_id'=>$user['id']]);
            
                            foreach($stmt as $row){
                                $stmt = $conn->prepare("INSERT INTO details (sales_id, product_id, quantity) VALUES (:sales_id, :product_id, :quantity)");
                                $stmt->execute(['sales_id'=>$salesid, 'product_id'=>$row['product_id'], 'quantity'=>$row['quantity']]);
                            }
            
                            $stmt = $conn->prepare("DELETE FROM cart WHERE user_id=:user_id");
                            $stmt->execute(['user_id'=>$user['id']]);
            
                            $_SESSION['success'] = "";
            
                        }
                        catch(PDOException $e){
                            $_SESSION['error'] = $e->getMessage();
                        }
            
                    }
                    catch(PDOException $e){
                        $_SESSION['error'] = $e->getMessage();
                    }   
                }else{
                    $_SESSION['error'] = "";
                }
             }catch(PDOException $e){
                    $_SESSION['error'] = $e->getMessage();
                }
        }else{
            try {
                if($row['numrows'] > 0){
                    try{
                        $stmt = $conn->prepare("INSERT INTO orders (user_id, pay_id, address, order_type, contacts, company, order_date, order_status) VALUES (:user_id, :pay_id, :address, :order_type, :contacts, :company, :order_date, :order_status)");
                        $stmt->execute(['user_id'=>$user['id'], 'pay_id'=>$payid, 'address'=>$address, 'order_type'=>$orderType, 'contacts'=>$contacts, 'company'=>$company, 'order_date'=>$date, 'order_status'=>$order_status]);
                        $salesid = $conn->lastInsertId();
                        
                        try{
                            $stmt = $conn->prepare("SELECT * FROM cart LEFT JOIN products ON products.id=cart.product_id WHERE user_id=:user_id");
                            $stmt->execute(['user_id'=>$user['id']]);
            
                            foreach($stmt as $row){
                                $stmt = $conn->prepare("INSERT INTO details (sales_id, product_id, quantity) VALUES (:sales_id, :product_id, :quantity)");
                                $stmt->execute(['sales_id'=>$salesid, 'product_id'=>$row['product_id'], 'quantity'=>$row['quantity']]);
                            }
            
                            $stmt = $conn->prepare("DELETE FROM cart WHERE user_id=:user_id");
                            $stmt->execute(['user_id'=>$user['id']]);
            
                            $_SESSION['success'] = '';
            
                        }
                        catch(PDOException $e){
                            $_SESSION['error'] = $e->getMessage();
                        }
            
                    }
                    catch(PDOException $e){
                        $_SESSION['error'] = $e->getMessage();
                    }
                }else{
                    $_SESSION['error'] = "";
                }
             }catch(PDOException $e){
                    $_SESSION['error'] = $e->getMessage();
                }
        }

		$pdo->close();
	}
	
	header('location: order_placed.php');
	
?>