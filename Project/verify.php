<?php
	include 'includes/session.php';
	$conn = $pdo->open();

	if(isset($_POST['login'])){
		
		$email = $_POST['email'];
		$password = $_POST['password'];

		try{

			$stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM users WHERE email = :email");
			$stmt->execute(['email'=>$email]);
			$row = $stmt->fetch();
			if($row['numrows'] > 0){
				if($row['status']){
					if(password_verify($password, $row['password'])){
						if($row['type'] == 0){
							$_SESSION['user'] = $row['id'];
							if(!empty($_POST['redir'])){
								header("Location:".$_POST['redir']);
							} else {
								header("location: profile.php");
							}
						}
						elseif($row['type'] == 1){
							$_SESSION['admin'] = $row['id'];
							header("Location: admin/index.php");
						}
						elseif($row['type'] == 2){
							$_SESSION['provider'] = $row['id'];
							header("Location: provider/index.php");
						}
					}
					else{
						$_SESSION['error'] = 'Incorrect Password';
						if(!empty($_POST['redir'])){
							header("Location: login.php?redir=".urlencode($_POST['redir']));
						} else {
							header("location: login.php");
						}
					}
				}
				else{
					$_SESSION['error'] = 'Account not activated.';
					if(!empty($_POST['redir'])){
						header("Location: login.php?redir=".urlencode($_POST['redir']));
					} else {
						header("location: login.php");
					}
				}
			}
			else{
				$_SESSION['error'] = 'Email not found';
				if(!empty($_POST['redir'])){
					header("Location: login.php?redir=".urlencode($_POST['redir']));
				} else {
					header("location: login.php");
				}
			}
		}
		catch(PDOException $e){
			echo "There is some problem in connection: " . $e->getMessage();
		}

	}
	else{
		$_SESSION['error'] = 'Input login credentails first';
		header('location: login.php');
	}
	$pdo->close();
?>
