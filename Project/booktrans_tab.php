<div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-calendar"></i> <b>Booking Transaction History</b></h4>
</div>
<div class="box-body">
    <table class="table table-bordered" id="example2" data-order='[[0, "desc"]]'>
        <thead>
            <th class="hidden">Booking ID</th>
            <th>Booking Date</th>
            <th>Transaction#</th>
            <th>Amount</th>
            <th>Full Details</th>
        </thead>
    <tbody>
        <?php
            $conn = $pdo->open();

            try{
                $stmt = $conn->prepare("SELECT * FROM bookings WHERE customer_id=:user_id ORDER BY id DESC");
                $stmt->execute(['user_id'=>$user['id']]);
                foreach($stmt as $row){
                    $stmt2 = $conn->prepare("SELECT * FROM services WHERE id=:service_id");
                    $stmt2->execute(['service_id'=>$row['services_id']]);
                    $service_name = $stmt2->fetch();
                    echo "
                        <tr>
                            <td class='hidden'>".$row['id']."</td>
                            <td>".date('M d, Y', strtotime($row['date']))."</td>
                            <td>".$row['bookingtrans']."</td>
                            <td>&#8369; ".number_format($service_name['price'], 2)."</td>
                            <td><button class='btn btn-sm btn-flat btn-info booktrans' data-id='".$row['id']."'><i class='fa fa-search'></i> View</button></td>
                        </tr>
                    ";
                }

            }
            catch(PDOException $e){
                echo "There is some problem in connection: " . $e->getMessage();
            }

            $pdo->close();
        ?>
        </tbody>
    </table>
</div>