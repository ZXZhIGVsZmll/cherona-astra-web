<?php
	include 'includes/session.php';

	$bookid = $_POST['bookid'];
    $fullname = $_POST['fullname'];
	$booktransaction = $_POST['booktransaction'];
	$date = date('Y-m-d');

    if(isset($_POST['book'])){
        $conn = $pdo->open();
        try{
            $stmt = $conn->prepare("UPDATE bookings SET booking_status=:booking_status WHERE id=:id");
            $stmt->execute(['booking_status'=>"booked", 'id'=>$bookid]);

            $_SESSION['booked'] = 'Booked ID: '.$bookid.' <br> Booking Transaction #: '.$booktransaction.' <br> Customer Name: '.$fullname.' ';
        }
        catch(PDOException $e){
            $_SESSION['error'] = $e->getMessage();
        }
        $pdo->close();
    }
    if(isset($_POST['reject'])){
        $conn = $pdo->open();
        try{
            $stmt = $conn->prepare("UPDATE bookings SET booking_status=:booking_status WHERE id=:id");
            $stmt->execute(['booking_status'=>"reject", 'id'=>$bookid]);

            $_SESSION['rejected'] = 'Booked ID: '.$bookid.' <br> Booking Transaction #: '.$booktransaction.' <br> Customer Name: '.$fullname.' ';
        }
        catch(PDOException $e){
            $_SESSION['error'] = $e->getMessage();
        }
        $pdo->close();
    }
    if(isset($_POST['done'])){
        $conn = $pdo->open();
        try{
            $stmt = $conn->prepare("UPDATE bookings SET booking_status=:booking_status WHERE id=:id");
            $stmt->execute(['booking_status'=>"done", 'id'=>$bookid]);

            $_SESSION['done'] = 'Booked ID: '.$bookid.' <br> Booking Transaction #: '.$booktransaction.' <br> Customer Name: '.$fullname.' ';
        }
        catch(PDOException $e){
            $_SESSION['error'] = $e->getMessage();
        }
        $pdo->close();
    }

	header('location: bookings.php');
?>