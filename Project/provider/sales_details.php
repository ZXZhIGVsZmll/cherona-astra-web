<?php
	include 'includes/session.php';

	$id = $_POST['id'];

	$conn = $pdo->open();

	$output = array('list'=>'');


    $stmt_admin = $conn->prepare("SELECT * FROM services WHERE service_admin_id=:service_id");
    $stmt_admin->execute(['service_id'=>$admin['id']]);
    $service_admin = $stmt_admin->fetch();

    $stmt = $conn->prepare("SELECT * FROM booking_sales WHERE id=:id");
    $stmt->execute(['id'=>$id]);
    $sales_admin = $stmt->fetch();


    $stmt2 = $conn->prepare("SELECT * FROM bookings WHERE id=:id");
    $stmt2->execute(['id'=>$sales_admin['booking_id']]);
    foreach($stmt2 as $sales){
        $stmt3 = $conn->prepare("SELECT * FROM users WHERE id=:id");
        $stmt3->execute(['id'=>$sales['customer_id']]);
        foreach ($stmt3 as $customer) {
            $fullname = $customer['firstname'].' '.$customer['lastname'];
        }
        $output['sales_date'] = date('M d, Y', strtotime($sales_admin['sales_date']));
        $output['bookingtrans'] = $sales['bookingtrans'];
        $output['fullname'] = $fullname;
        $output['contact'] = $sales['phone'];
        $output['address'] = $sales['address'];
        $output['confirmed_by'] = $sales['confirmed_by'];
        $total = $service_admin['price'];
        $output['list'] .= "
            <tr class='prepend_items'>
                <td>".date('M d, Y', strtotime($sales['date']))."</td>
                <td>".$service_admin['name']."</td>
                <td>&#8369;$total</td>
            </tr>
        ";
    }
	$output['total'] = '<b>&#8369;'.number_format($total, 2).'<b>';
	$pdo->close();
	echo json_encode($output);

?>