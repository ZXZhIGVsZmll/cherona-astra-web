<?php
	include 'includes/session.php';

	$id = $_POST['id'];

	$conn = $pdo->open();

	$output = array('list'=>'');

    $stmt = $conn->prepare("SELECT *, services.id AS servicesid, bookings.id AS bookid FROM bookings LEFT JOIN services ON services.id=bookings.services_id LEFT JOIN users ON users.id=bookings.customer_id WHERE bookings.id=:id");
    $stmt->execute(['id'=>$id]);
    foreach($stmt as $row){
        $output['bookid'] = $row['bookid'];
        $output['services_id'] = $row['servicesid'];
        $output['booktransaction'] = $row['bookingtrans'];
        $output['service_name'] = $row['name'];
        $output['fullname'] = $row['firstname'] .' '. $row['lastname'];
        $output['booking_date'] = date('M d, Y', strtotime($row['date']));
        $output['price'] = '<b>&#8369;'.number_format($row['price'], 2).'<b>';
    }
	$pdo->close();
	echo json_encode($output);

?>