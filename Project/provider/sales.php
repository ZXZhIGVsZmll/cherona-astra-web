<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Booking Sales History
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Booking Sales</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered" data-order='[[0,"desc"]]'>
                <thead>
                  <th class="hidden">Sales ID</th>
                  <th>Sales Date</th>
                  <th>Booking Transaction #</th>
                  <th>Customer Name</th>
                  <th>Customer Contact</th>
                  <th>Full Details</th>

                </thead>
                <tbody>
                  <?php
                    $conn = $pdo->open();

                    $stmt = $conn->prepare("SELECT * FROM services WHERE service_admin_id=:service_id");
                    $stmt->execute(['service_id'=>$admin['id']]);
                    $service_admin = $stmt->fetch();

                    try{
                      $stmt = $conn->prepare("SELECT *, booking_sales.id AS sales_id FROM booking_sales LEFT JOIN bookings ON bookings.id=booking_sales.booking_id WHERE bookings.services_id=:service_id ORDER BY booking_sales.sales_date DESC");
                      $stmt->execute(['service_id'=>$service_admin['id']]);
                      foreach($stmt as $row){
                        $stmt2 = $conn->prepare("SELECT * FROM bookings LEFT JOIN users ON users.id=bookings.customer_id LEFT JOIN services ON services.id=bookings.services_id WHERE bookings.customer_id=:customer_id");
                        $stmt2->execute(['customer_id'=>$row['customer_id']]);
                        foreach($stmt2 as $customer_info){
                          $fullname = $customer_info['firstname'].' '.$customer_info['lastname'];
                        }
                        echo "
                          <tr>
                            <td class='hidden'>".$row['sales_id']."</td>
                            <td>".date('M d, Y', strtotime($row['sales_date']))."</td>
                            <td>".$row['bookingtrans']."</td>
                            <td>$fullname</td>
                            <td>".$row['phone']."</td>
                            <td><button type='button' class='btn btn-info btn-sm btn-flat details' data-id='".$row['sales_id']."'><i class='fa fa-search'></i> View Details</button></td>
                          </tr>
                        ";
                      }
                    }
                    catch(PDOException $e){
                      echo $e->getMessage();
                    }

                    $pdo->close();
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     
  </div>
  	<?php include 'includes/footer.php'; ?>
    <?php include 'includes/bookings_modal.php'; ?>

</div>
<!-- ./wrapper -->

<?php include 'includes/scripts.php'; ?>
<!-- Date Picker -->
<script>
$(function(){
  //Date picker
  $('#datepicker_add').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#datepicker_edit').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )
  
});
</script>
<script>
$(function(){
  $(document).on('click', '.details', function(e){
    e.preventDefault();
    $('#service_details').modal('show');
    var id = $(this).data('id');
    $.ajax({
      type: 'POST',
      url: 'sales_details.php',
      data: {id:id},
      dataType: 'json',
      success:function(response){
        $('#address').html(response.address);
        $('#customer_name').html(response.fullname);
        $('#sales_date').html(response.sales_date);
        $('#transid').html(response.bookingtrans);
        $('#customer_contact').html(response.contact);
        $('#cherona_admin').html(response.confirmed_by);
        $('#total').html(response.total);
        $('#detail').prepend(response.list);
      }
    });
  });

  $("#service_details").on("hidden.bs.modal", function () {
      $('.prepend_items').remove();
  });
});
</script>
</body>
</html>
