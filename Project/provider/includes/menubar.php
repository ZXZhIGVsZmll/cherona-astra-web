<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
			<img src="<?php echo (!empty($admin['photo'])) ? '../images/'.$admin['photo'] : '../images/profile.jpg'; ?>" class="img-circle" alt="User Image" style="margin-bottom: 2rem;">
			</div>
			<div class="pull-left info">
			<p><?php echo $admin['id'] .' '. $admin['firstname'].' '.$admin['lastname']; ?></p>
			<a><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">REPORTS</li>
			<li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="sales.php"><i class="fa fa-money"></i> <span>Service Sales</span></a></li>
			<li class="header">MANAGE</li>
			<li><a href="bookings.php"><i class="far fa-calendar-check"></i> <span> Bookings</span>
					<small class="badge pull-right bg-orange" data-toggle="tooltip" data-placement="top" title="Pending Bookings">
						<?php
						$stmt = $conn->prepare("SELECT * FROM services WHERE service_admin_id=:service_admin");
						$stmt->execute(['service_admin'=>$admin['id']]);
						$service_admin = $stmt->fetch();	

							try {
								$stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM bookings WHERE booking_status='pending' AND services_id=:id");
								$stmt->execute(['id'=>$service_admin['id']]);
								$urow =  $stmt->fetch();
								echo "".$urow['numrows']."";
							} catch (PDOException $e) {
								echo $e->getMessage();
							}
						?>
					</small>
				</a>
			</li>
		</ul>
	</section>
<!-- /.sidebar -->
</aside>
