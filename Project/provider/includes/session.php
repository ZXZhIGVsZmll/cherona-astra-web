<?php
	include '../includes/conn.php';
	session_start();

	if(!isset($_SESSION['provider']) || trim($_SESSION['provider']) == ''){
		header('location: ../index.php');
		exit();
	}

	$conn = $pdo->open();

	$stmt = $conn->prepare("SELECT * FROM users WHERE id=:id");
	$stmt->execute(['id'=>$_SESSION['provider']]);
	$admin = $stmt->fetch();

	$pdo->close();

?>
