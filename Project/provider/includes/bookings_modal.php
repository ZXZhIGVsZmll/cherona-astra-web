<!-- Book -->
<div class="modal fade" id="book">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-booked-book">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Book Service?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="bookings_confirmation.php">
              <p>
                Booked ID: <span id="bookid_show"></span>
                <span class="pull-right">Date: <span id="booking_date"></span></span>
              </p>
              <p>
                Service Name: <span id="service_name"></span>
                <span class="pull-right">Amount: <span id="price"></span></span>
              </p>
                <input type="hidden" id="services_id" name="services_id">
                <input type="hidden" id="bookid" name="bookid">
                <input type="hidden" id="booktransaction" name="booktransaction">
                <input type="hidden" id="fullname" name="fullname">
                <div class="text-center">
                    <p> </p>
                    <h2 class="bold fullname"></h2>
                    <h2 class="lead booktransaction"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-booked-book btn-flat" name="book"><i class="fa fa-calendar"></i> Booked</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Done -->
<div class="modal fade" id="done">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Service already done?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="bookings_confirmation.php">
                <input type="hidden" id="done_services_id" name="services_id">
                <input type="hidden" id="done_bookid" name="bookid">
                <input type="hidden" id="done_booktransaction" name="booktransaction">
                <input type="hidden" id="done_fullname" name="fullname">
                <div class="text-center">
                    <p> </p>
                    <h2 class="bold fullname"></h2>
                    <h2 class="lead booktransaction"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="done"><i class="fa fa-check"></i> Done</button>
              </form>
            </div>
        </div>
    </div>
</div>

<!-- Reject -->
<div class="modal fade" id="reject">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Reject Booking Service?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="bookings_confirmation.php">
                <input type="hidden" id="reject_services_id" name="services_id">
                <input type="hidden" id="reject_bookid" name="bookid">
                <input type="hidden" id="reject_booktransaction" name="booktransaction">
                <input type="hidden" id="reject_fullname" name="fullname">
                <div class="text-center">
                    <p> </p>
                    <h2 class="bold fullname"></h2>
                    <h2 class="lead booktransaction"></h2>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-danger btn-flat" name="reject"><i class="fa fa-close"></i> Reject</button>
              </form>
            </div>
        </div>
    </div>
</div>


<!-- Transaction History -->
<div class="modal fade" id="service_details">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Booked Service Full Details</b></h4>
            </div>
            <div class="modal-body">
              <p>
                Sales Date: <span id="sales_date"></span>
                <span class="pull-right">Transaction #: <span id="transid"></span></span> 
              </p>
              <p>
                Customer Name: <span id="customer_name"></span>
                <span class="pull-right">Customer Contact: <span id="customer_contact"></span></span> 
              </p>
              <p>
                Address: <span id="address"></span>
              </p>
              <table class="table table-bordered">
                  <thead>
                    <th>Booking Date</th>
                    <th>Service Name</th>
                    <th>Amount</th>
                  </thead>
                  <tbody id="detail">
                    <tr>
                      <td colspan="2" align="right"><b>Total Amount</b></td>
                      <td><span id="total"></span></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <p>
                  <span class="pull-right">Payment Confirmed: <span id="cherona_admin"></span></span>
                </p>
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>
