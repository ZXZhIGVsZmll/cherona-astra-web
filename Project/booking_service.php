<?php
include 'includes/session.php';

if(isset($_POST['book'])){
    $booking_date = $_POST['booking_date'];

    $date = date('Y-m-d', strtotime($booking_date));

    $set = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $temp_trans = substr(str_shuffle($set), 0, 24);

    $booktrans = "BID-$temp_trans";
    $userid = $_POST['user_id'];
    $servicesid = $_POST['services_id'];
    $address = $_POST['address'];
    $phoneno = $_POST['contacts'];
    

    $conn = $pdo->open();

    $stmt = $conn->prepare("INSERT INTO bookings (bookingtrans, services_id, customer_id, address, date, phone, booking_status) VALUES (:bookingid, :servicesid, :customer_id, :address, :date, :phone, :booking_status)");
    $stmt->execute(['bookingid'=>$booktrans, 'servicesid'=>$servicesid, 'customer_id'=>$userid, 'address'=>$address, 'date'=>$date, 'phone'=>$phoneno, 'booking_status'=>'pending']);

    $_SESSION['success'] = '';

    $pdo->close();
}

header('location: service_booked.php');

?>