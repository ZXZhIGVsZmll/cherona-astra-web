<div class="box box-solid">	
    <div class="box-body">
        <form method="POST" action="order.php">
            <h4>Delivery Method</h4>
            <div class="form-horizontal">
                <div class="radio">
                    <label>
                        <input type="radio" name="radio" id="ship" value="ship" onclick="enableForm()" checked>
                        <i class="fas fa-truck"></i> Ship To
                    </label>
                </div>
                <div class="radio">
                <label>
                    <input type="radio" name="radio" id="pick-up" value="pick-up" onclick="disableForm()">
                    <i class="fas fa-store"></i> Store Pick-Up
                </label>
                </div>
            </div>
            <hr>
            <h4>Shipping Information</h4>
                <div class="form-group">
                    <label class="sr-only" for="inputCompany">Company (Optional)</label>
                    <input type="text" name="company" class="form-control" id="inputCompany" placeholder="Company (Optional)" autocomplete="false">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="inputAddress">Address</label>
                    <input type="text" name="address" class="form-control" id="inputAddress" placeholder="Address" autocomplete="false" value="<?php echo $user['address']; ?>" required>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="inputEtc">Apartment, Suite etc. (Optional)</label>
                    <input type="text" name="addExt" class="form-control" id="inputEtc" placeholder="Apartment, Suite etc. (Optional)" autocomplete="false">
                </div>
            <div class="row">
                <div class="col-lg-4" style="padding-right: 1px;">
                        <label class="sr-only" for="inputPostal">Postal Code</label>
                        <input type="text" class="form-control whole-number" name="postal" id="inputPostal" placeholder="Postal Code" maxlength="4" required>
                </div>
                <div class="col-lg-4" style="padding-right: 1px;">
                        <label class="sr-only" for="inputCity">City</label>
                        <input type="text" class="form-control" name="city" id="inputCity" placeholder="City" autocomplete="false" required>
                </div>
                <div class="col-lg-4" data-tip="Use 09xx format">
                        <label class="sr-only" for="inputPhone">Phone Number</label>
                        <input type="text" class="form-control contact-number" name="contacts" id="inputPhone" placeholder="Contact Number: 09 ..." autocomplete="false" value="<?php echo $user['contact_info']; ?>" required>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-flat" name="checkout" id="checkout"><i class="fa fa-shopping-cart"></i> Checkout</button>
            </div>
        </form>
    </div>
</div>
