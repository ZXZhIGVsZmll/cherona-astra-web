<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

	<?php include 'includes/navbar.php'; ?>
	 
	  <div class="content-wrapper">
	    <div class="container">

	      <!-- Main content -->
	      <section class="content">
	        <div class="row">
	        	<div class="col-sm-9">
	            <?php
				//$_GET['keyword'] = "";
				// Count
				$conn = $pdo->open();
				$stmt = $conn->prepare("SELECT COUNT(*) AS numrows FROM products WHERE name LIKE :keyword");
				$stmt->execute(['keyword' => '%'.$_GET['keyword'].'%']);
				$row = $stmt->fetch();

				$stmtServ = $conn->prepare("SELECT COUNT(*) AS numrows FROM services WHERE name LIKE :keyword");
				$stmtServ->execute(['keyword' => '%'.$_GET['keyword'].'%']);
				$rowServ = $stmtServ->fetch();

				// Show results
				if($row['numrows'] > 0 || $rowServ['numrows'] > 0){
					echo '<h1 class="page-header">Search results for <i>'.$_GET['keyword'].'</i></h1>';
					try{
						$inc = 3;
						$stmt = $conn->prepare("SELECT * FROM products WHERE name LIKE :keyword");
						$stmt->execute(['keyword' => '%'.$_GET['keyword'].'%']);
						foreach ($stmt as $row) {
							$highlighted = preg_filter('/' . preg_quote($_GET['keyword'], '/') . '/i', '<b>$0</b>', $row['name']);
							$image = (!empty($row['photo'])) ? 'images/'.$row['photo'] : 'images/noimage.jpg';
							$inc = ($inc == 3) ? 1 : $inc + 1;
							if($inc == 1) echo "<div class='row'>";
							echo "
							<div class='col-sm-4'>
								<div class='box box-solid'>
									<a href='product.php?product=".$row['slug']."'>
										<div class='box-body prod-body'>
											<img src='".$image."' width='100%' height='230px' class='thumbnail'>
											<h5>".$highlighted."</h5>
										</div>
										<div class='box-footer'>
											<b>&#8369; ".number_format($row['price'], 2)."</b>
										</div>
									</a>
								</div>
							</div>
							";
							if($inc == 3) echo "</div>";
							}
							if($inc == 1) echo "<div class='col-sm-4'></div><div class='col-sm-4'></div></div>"; 
							if($inc == 2) echo "<div class='col-sm-4'></div></div>";
					} catch(PDOException $e){
						echo "There is some problem in connection: " . $e->getMessage();
						}
					try{
						$inc = 3;
						$stmt = $conn->prepare("SELECT * FROM services WHERE name LIKE :keyword");
						$stmt->execute(['keyword' => '%'.$_GET['keyword'].'%']);
						foreach ($stmt as $row) {
							$highlighted = preg_filter('/' . preg_quote($_GET['keyword'], '/') . '/i', '<b>$0</b>', $row['name']);
							$image = (!empty($row['photo'])) ? 'images/'.$row['photo'] : 'images/noimage.jpg';
							$inc = ($inc == 3) ? 1 : $inc + 1;
							if($inc == 1) echo "<div class='row'>";
							echo "
							<div class='col-sm-4'>
								<div class='box box-solid'>
									<a href='service.php?product=".$row['slug']."'>
										<div class='box-body prod-body'>
											<img src='".$image."' width='100%' height='230px' class='thumbnail'>
											<h5>".$highlighted."</h5>
										</div>
										<div class='box-footer'>
											<b>&#8369; ".number_format($row['price'], 2)."</b>
										</div>
									</a>
								</div>
							</div>
							";
							if($inc == 3) echo "</div>";
							}
							if($inc == 1) echo "<div class='col-sm-4'></div><div class='col-sm-4'></div></div>"; 
							if($inc == 2) echo "<div class='col-sm-4'></div></div>";
					}
					catch(PDOException $e){
						echo "There is some problem in connection: " . $e->getMessage();
						}
				} else {
					echo '<h1 class="page-header">No results found for <i>'.$_GET['keyword'].'</i></h1>';
				}
				$pdo->close();
				?> 
	        	</div>
	        	<div class="col-sm-3">
	        		<?php include 'includes/sidebar.php'; ?>
	        	</div>
	        </div>
	      </section>
	     
	    </div>
	  </div>
  
  	<?php include 'includes/footer.php'; ?>
</div>

<?php include 'includes/scripts.php'; ?>
</body>
</html>