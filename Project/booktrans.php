<?php
	include 'includes/session.php';

	$id = $_POST['id'];

    $conn = $pdo->open();

	$output = array('list'=>'');

        $stmt = $conn->prepare("SELECT * FROM bookings WHERE id=:id");
        $stmt->execute(['id'=>$id]);
        foreach($stmt as $row){
            $stmt2 = $conn->prepare("SELECT * FROM services WHERE id=:service_id");
            $stmt2->execute(['service_id'=>$row['services_id']]);
            $service_name = $stmt2->fetch();

            $output['book_date'] = date('M d, Y', strtotime($row['date']));
            $output['booktrans'] = $row['bookingtrans'];
            $output['cherona_admin'] = $row['confirmed_by'];
            $output['bookstats'] = $row['booking_status'];
            $total = $service_name['price'];
            $output['list'] .= "
			<tr class='prepend_items'>
				<td>".$service_name['name']."</td>
                <td>".$row['address']."</td>
                <td>".$row['phone']."</td>
                <td>&#8369;".number_format($total, 2)."</td>
			</tr>
		    ";
        }

        if($row['booking_status']=='reject'){
            $bookstats = '<span class="label label-danger">rejected</span>';
        }else{
            $bookstats = '<span class="label label-success">paid</span>';
        }
    
    $output['bookstats'] = $bookstats;
    $output['total'] = '<b>&#8369;'.number_format($total, 2).'<b>';
	$pdo->close();
	echo json_encode($output);

?>