<?php include 'includes/session.php'; ?>
<?php include 'includes/unset_sessions.php'; ?>
<?php
$conn = $pdo->open();
$slug = $_GET['product'];
try{	 		
	$stmt = $conn->prepare("SELECT *, services.name AS prodname, service_category.name AS catname, services.id AS prodid FROM services LEFT JOIN service_category ON service_category.id=services.category_id WHERE slug = :slug");
	$stmt->execute(['slug' => $slug]);
	$product = $stmt->fetch();	
}catch(PDOException $e){
	echo "There is some problem in connection: " . $e->getMessage();
}
//page view
$now = date('Y-m-d');
if($product['date_view'] == $now){
	$stmt = $conn->prepare("UPDATE services SET counter=counter+1 WHERE id=:id");
	$stmt->execute(['id'=>$product['prodid']]);
}else{
	$stmt = $conn->prepare("UPDATE services SET counter=1, date_view=:now WHERE id=:id");
	$stmt->execute(['id'=>$product['prodid'], 'now'=>$now]);
}
?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue layout-top-nav">
<script>
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="wrapper">
	<?php include 'includes/navbar.php'; ?> 
	  <div class="content-wrapper">
	    <div class="container">
	      <!-- Main content -->
	      <section class="content">
	        <div class="row">
	        	<div class="col-sm-9">
	        		<div class="callout" id="callout" style="display:none">
	        			<button type="button" class="close"><span aria-hidden="true">&times;</span></button>
	        			<span class="message"></span>
	        		</div>
		            <div class="row">
		            	<div class="col-sm-6">
		            		<img src="<?php echo (!empty($product['photo'])) ? 'images/'.$product['photo'] : 'images/noimage.jpg'; ?>" width="100%" class="zoom" data-magnify-src="images/large-<?php echo $product['photo']; ?>">
		            		<br><br>
		            		<form class="form-inline" id="productForm">
		            			<div class="form-group">
									<?php
										if(!isset($_SESSION['user'])){
											$redir=urlencode($_SERVER['REQUEST_URI']);
											echo "You must <a href=\"login.php?redir=".$redir."\">LOGIN</a> first";
										}else{
											echo '<button type="button" class="btn btn-booked-book btn-lg btn-flat book" data-id="'.$product['prodid'].'"><i class="fa fa-calendar"></i> Book A Service</button>';
										}
									?>
			            		</div>

		            		</form>
		            	</div>
		            	<div class="col-sm-6">
		            		<h1 class="page-header"><?php echo $product['prodname']; ?></h1>
		            		<h3><b>&#8369 <?php echo number_format($product['price'], 2); ?></b></h3>
		            		<p><b>Category:</b> <a href="services.php?category=<?php echo $product['cat_slug']; ?>"><?php echo $product['catname']; ?></a></p>
		            		<p><b>Description:</b></p>
		            		<p><?php echo $product['description']; ?></p>
		            	</div>
		            </div>
		            <br>
					<!-- Uncomment to enable fb comments -->
				    <!--div class="fb-comments" data-href="http://localhost/ecommerce/product.php?product=<?php // echo $slug; ?>" data-numposts="10" width="100%"></div--> 
	        	</div>
	        	<div class="col-sm-3">
	        		<?php include 'includes/sidebar.php'; ?>
	        	</div>
	        </div>
	      </section>
	    </div>
	  </div>
  	<?php $pdo->close(); ?>
  	<?php include 'includes/footer.php'; ?>
	  <?php include 'includes/bookings_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>
<!-- Date Picker -->
<script>
	var dateToday = new Date();
	dateToday.setDate(dateToday.getDate() + 1);
$(function(){
  //Date picker
  $('#datepicker_add').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#datepicker_edit').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })

  //Date range picker
  $('#reservation').daterangepicker({
	  "singleDatePicker": true,
	  minDate: dateToday
  })
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )
  
});
</script>
<script>
$(function(){
	$(document).on('click', '.book', function(e){
		e.preventDefault();
		$("#book").modal('show');
		var id = $(this).data('id');
		$.ajax({
			type: 'POST',
			url: 'booking_details.php',
			data: {id,id},
			dataType: 'json',
			success:function(response){
				$('#fullname').html(response.fullname);
				$('#service_name').html(response.service_name);
				$('#service_price').html(response.service_price);
				$('.user_id').val(response.user_id);
				$('.firstname').val(response.firstname);
				$('.lastname').val(response.lastname);
				$('.services_id').val(response.services_id);
			}
		});
	});
});
</script>
</body>
</html>
