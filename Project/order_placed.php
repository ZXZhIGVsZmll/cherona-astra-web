<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

	<?php include 'includes/navbar.php'; ?>
	 
	  <div class="content-wrapper">
	    <div class="container">

	      <!-- Main content -->
	      <section class="content">
	        <div class="row">
	        	<div class="col-sm-9">
                <?php
					if(isset($_SESSION['error'])){
						include 'includes/empty_cart.php';
					}
					if(isset($_SESSION['success'])){
						include 'includes/pay_process.php';
					}
					if(!isset($_SESSION['success'])&&!isset($_SESSION['error'])){
						include 'includes/empty_cart.php';
					}	
				?>
	        	</div>
	        	<div class="col-sm-3">
	        		<?php include 'includes/sidebar.php'; ?>
	        	</div>
	        </div>
	      </section>
		</div>
	    </div>
	  </div>

  	<?php include 'includes/footer.php'; ?>
</div>

<?php include 'includes/scripts.php'; ?>
</body>
</html>