<?php
	include 'includes/session.php';

	if(isset($_POST['checkout'])){
		$date = date('Y-m-d');

		//generate code
		$set='123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$temp_pay=substr(str_shuffle($set), 0, 24);
		$payid = "OID-$temp_pay";
		$orderType = $_POST['radio'];
		$company = $_POST['company'];
		$address = $_POST['address'] . ' ' . $_POST['addExt'] . ' ' . $_POST['city'] . ' ' . $_POST['postal'];
		$contacts = $_POST['contacts'];
		$status = "pending";
		$notif = "unread";


		$conn = $pdo->open();

		try{
			
			$stmt = $conn->prepare("INSERT INTO sales (user_id, pay_id, sales_date) VALUES (:user_id, :pay_id, :sales_date)");
			$stmt->execute(['user_id'=>$user['id'], 'pay_id'=>$payid, 'sales_date'=>$date]);
			$salesid = $conn->lastInsertId();

			// $stmt = $conn->prepare("INSERT INTO orders (user_id, pay_id, address, order_type, contacts, company, order_date, status, notif) VALUES (:user_id, :pay_id, :address, :order_type, :contacts, :company, :order_date, :status, :notif)");
			// $stmt->execute(['user_id'=>$user['id'], 'pay_id'=>$payid, 'address'=>$address, 'order_type'=>$orderType, 'contacts'=>$contacts, 'company'=>$company, 'order_date'=>$date, 'status'=>$status, 'notif'=>$notif]);
			// $salesid = $conn->lastInsertId();
			
			try{
				$stmt = $conn->prepare("SELECT * FROM cart LEFT JOIN products ON products.id=cart.product_id WHERE user_id=:user_id");
				$stmt->execute(['user_id'=>$user['id']]);

				foreach($stmt as $row){
					$stmt = $conn->prepare("INSERT INTO details (sales_id, product_id, quantity) VALUES (:sales_id, :product_id, :quantity)");
					$stmt->execute(['sales_id'=>$salesid, 'product_id'=>$row['product_id'], 'quantity'=>$row['quantity']]);
				}

				$stmt = $conn->prepare("DELETE FROM cart WHERE user_id=:user_id");
				$stmt->execute(['user_id'=>$user['id']]);

				$_SESSION['success'] = 'Transaction successful. Thank you.'.$salesid;

			}
			catch(PDOException $e){
				$_SESSION['error'] = $e->getMessage();
			}

		}
		catch(PDOException $e){
			$_SESSION['error'] = $e->getMessage();
		}

		$pdo->close();
	}
	
	header('location: profile.php');
	
?>