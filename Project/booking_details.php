<?php
	include 'includes/session.php';

	$id = $_POST['id'];


	$conn = $pdo->open();

	$output = array('list'=>'');

	$stmt = $conn->prepare("SELECT * FROM services WHERE id=:id");
	$stmt->execute(['id'=>$id]);

	$total = 0;
	foreach($stmt as $row){
        $output['fullname'] = '<b>'.$user['firstname'].' '.$user['lastname'].'</b>';
        $output['firstname'] = $user['firstname'];
        $output['lastname'] = $user['lastname'];
        $output['user_id'] = $user['id'];
		$output['service_name'] = '<b>'.$row['name'].'</b>';
        $output['services_id'] = $id;
		$service_price = $row['price'];
	}
	
	$output['service_price'] = '<b>&#8369;'.number_format($service_price, 2).'<b>';
	$pdo->close();
	echo json_encode($output);

?>